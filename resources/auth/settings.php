<?php

use Nrg\Auth\Abstraction\AccessTokenGenerator;
use Nrg\Auth\Action\Permission\PermissionList;
use Nrg\Auth\Action\Role\Details\RoleDetailsUpdate;
use Nrg\Auth\Action\Role\RoleCreate;
use Nrg\Auth\Action\Role\RoleDelete;
use Nrg\Auth\Action\Role\RoleDetails;
use Nrg\Auth\Action\Role\RoleList;
use Nrg\Auth\Action\Token\TokenRenew;
use Nrg\Auth\Action\User\ProfileUploadAvatar;
use Nrg\Auth\Action\User\UserActivate;
use Nrg\Auth\Action\User\UserAvatar;
use Nrg\Auth\Action\User\UserCreate;
use Nrg\Auth\Action\User\UserDetails;
use Nrg\Auth\Action\User\UserList;
use Nrg\Auth\Action\User\UserLogin;
use Nrg\Auth\Action\User\UserLogout;
use Nrg\Auth\Action\User\UserRolesAssign;
use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\Abstraction\AvatarStorage;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Persistence\Repository\DbalRoleRepository;
use Nrg\Auth\Persistence\Repository\DbalTokenRepository;
use Nrg\Auth\Persistence\Repository\DbalUserRepository;
use Nrg\Auth\Service\LocalAvatarStorage;
use Nrg\Auth\Service\PseudoRandomAccessTokenGenerator;
use Nrg\Auth\Service\TokenBasedAccessControl;

return [
    'routes' => [
        '/auth/permission/list' => PermissionList::class,

        '/auth/role/list' => RoleList::class,
        '/auth/role/create' => RoleCreate::class,
        '/auth/role/update' => RoleDetailsUpdate::class,
        '/auth/role/delete' => RoleDelete::class,
        '/auth/role/details' => RoleDetails::class,

        '/auth/user/list' => UserList::class,
        '/auth/user/create' => UserCreate::class,
        '/auth/user/details' => UserDetails::class,
        '/auth/user/roles/assign' => UserRolesAssign::class,
        '/auth/user/activate' => UserActivate::class,
        '/auth/user/profile/upload-avatar' => ProfileUploadAvatar::class,
        'GET:/auth/user/avatar' => UserAvatar::class,

        '/auth/token/login' => UserLogin::class,
        '/auth/token/logout' => UserLogout::class,
        '/auth/token/renew' => TokenRenew::class,
    ],
    'services' => [
        RoleRepository::class => DbalRoleRepository::class,
        UserRepository::class => DbalUserRepository::class,
        TokenRepository::class => DbalTokenRepository::class,
        AccessControl::class => TokenBasedAccessControl::class,
        AccessTokenGenerator::class => PseudoRandomAccessTokenGenerator::class,
        AvatarStorage::class => LocalAvatarStorage::class,
    ],
    'config' => [
        AccessControl::class => [
            'freeAccessRoutes' => [
                '/',
                '/auth/token/login',
                '/auth/user/activate',
            ],
            'authorizedOnlyRoutes' => [
                '/auth/token/logout',
                '/auth/token/renew'
            ]
        ],
        AccessTokenGenerator::class => [
            'tokenLength' => 128,
            'timeToLive' => '+7 days',
        ],
        AvatarStorage::class => [
            'path' => realpath(__DIR__.'/avatar-storage'),
            'route' => '/auth/user/avatar',
        ],
    ]
];
