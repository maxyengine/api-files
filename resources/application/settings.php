<?php

use Nrg\Auth\Observer\HttpExchange\Authorization;
use Nrg\Data\Abstraction\SchemaAdapter;
use Nrg\Data\Service\CaseStyleSchemaAdapter;
use Nrg\Doctrine\Abstraction\Connection;
use Nrg\Doctrine\Connection\SqliteConnection;
use Nrg\Http\Abstraction\RouteProvider;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Observer\HttpExchange\CorsControl;
use Nrg\Http\Observer\HttpExchange\ExceptionHandler;
use Nrg\Http\Observer\HttpExchange\JsonRequestParser;
use Nrg\Http\Observer\HttpExchange\JsonResponseSerializer;
use Nrg\Http\Observer\HttpExchange\ResponseEmitter;
use Nrg\Http\Observer\HttpExchange\Controller;
use Nrg\Http\Service\HttpRouteProvider;
use Nrg\Http\UseCase\HandleCors;
use Nrg\Http\Value\HttpStatus;
use Nrg\I18n\Abstraction\Translator;
use Nrg\I18n\Service\I18nTranslator;
use Nrg\Rx\Abstraction\EntityEventManager;
use Nrg\Rx\Abstraction\EventProvider;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Nrg\Rx\Service\RxEntityEventManager;
use Nrg\Rx\Service\RxEventProvider;
use Nrg\Utility\Abstraction\Uuid;
use Nrg\Utility\Service\PseudoRandomUuid;

return [
    'routes' => [
        null => new class implements Observer
        {
            use ObserverStub;

            /**
             * @param HttpExchangeEvent $event
             */
            public function onNext($event)
            {
                $event->getResponse()->setStatus(new HttpStatus(HttpStatus::NOT_FOUND));
            }
        },
    ],
    'events' => [
        HttpExchangeEvent::class => [
            //ExceptionHandler::class,
            CorsControl::class,
            Authorization::class,
            JsonRequestParser::class,
            Controller::class,
            JsonResponseSerializer::class,
            ResponseEmitter::class,
        ],
    ],
    'services' => [
        EventProvider::class => RxEventProvider::class,
        RouteProvider::class => HttpRouteProvider::class,
        Connection::class => SqliteConnection::class,
        SchemaAdapter::class => CaseStyleSchemaAdapter::class,
        EntityEventManager::class => RxEntityEventManager::class,
        Uuid::class => PseudoRandomUuid::class,
        Translator::class => I18nTranslator::class,
    ],
    'config' => [
        Connection::class => [
            'path' => __DIR__.'/db/nrg.sqlite3',
        ],
        Translator::class => [
            //'locale' => 'en',
        ],
        HandleCors::class => [
            'headers' => [
                'Access-Control-Allow-Origin' => '*',
            ]
        ],
    ],
];
