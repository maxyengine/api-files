<?php

use Phinx\Seed\AbstractSeed;

class AuthSeeder extends AbstractSeed
{
    const USER_TABLE = 'nrgsoft_auth_user';
    const ROLE_TABLE = 'nrgsoft_auth_role';
    const USER_HAS_ROLE_TABLE = 'nrgsoft_auth_user_has_role';
    const LOCALE = 'ru_RU';

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $userTable = $this->table(self::USER_TABLE);
        $roleTable = $this->table(self::ROLE_TABLE);
        $userHasRoleTable = $this->table(self::USER_HAS_ROLE_TABLE);

        $faker = Faker\Factory::create(self::LOCALE);

        $userData = [
            'id' => $faker->uuid,
            'name' => 'Super',
            'email' => 'super@user.com',
            'status' => 'active',
            'description' => 'Super user',
            'permissions' => null,
            'password_hash' => password_hash('test7777', PASSWORD_DEFAULT),
            'created_at' => date("Y-m-d H:i:s"),
        ];

        $userTable->insert($userData)->save();

        $userData = [
            'id' => $faker->uuid,
            'name' => 'Admin',
            'email' => 'admin@app.com',
            'status' => 'active',
            'description' => 'With Admin role',
            'permissions' => json_encode([
                '/files/read', // for test user.isAllow() on client app
            ]),
            'password_hash' => password_hash('test7777', PASSWORD_DEFAULT),
            'created_at' => date("Y-m-d H:i:s"),
        ];

        $roleData = [
            'id' => $faker->uuid,
            'name' => 'Admin',
            'description' => 'Admin with certain permissions',
            'permissions' => json_encode(
                [
                    '/auth/permission/list',

                    '/auth/role/list',
                    '/auth/role/create',
                    '/auth/role/update',
                    '/auth/role/delete',
                    '/auth/role/details',

                    '/auth/user/list',
                    '/auth/user/create',
                    '/auth/user/details',
                    '/auth/user/roles/assign',
                    '/auth/user/activate',

                    '/auth/token/details',

                    '/files/create/directory',
                ]
            ),
            'created_at' => date("Y-m-d H:i:s"),
        ];

        $userHasRoleData = [
            'user_id' => $userData['id'],
            'role_id' => $roleData['id'],
        ];

        $userTable->insert($userData)->save();
        $roleTable->insert($roleData)->save();
        $userHasRoleTable->insert($userHasRoleData)->save();
    }
}
