<?php

use Phinx\Migration\AbstractMigration;

/**
 * Class NrgsoftFileManager
 */
class NrgsoftFileManager extends AbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function change()
    {
        $this->table('nrgsoft_file_manager_storage', ['id' => false])
            ->addColumn('id', 'uuid')
            ->addColumn('type', 'string', ['limit' => 20])
            ->addColumn('name', 'string', ['limit' => 50])
            ->addColumn('description', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('params', 'text')
            ->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['timezone' => true, 'null' => true])
            ->addIndex(['id'], ['unique' => true])
            ->addIndex(['name'], ['unique' => true])
            ->create();
    }
}
