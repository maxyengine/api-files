<?php

use Phinx\Migration\AbstractMigration;

/**
 * Class NrgsoftAuth.
 */
class NrgsoftAuth extends AbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function change()
    {
        $this->table('nrgsoft_auth_user', ['id' => false])
            ->addColumn('id', 'uuid')
            ->addColumn('name', 'string', ['limit' => 50])
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('status', 'string', ['limit' => 20])
            ->addColumn('password_hash', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('description', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('token', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('token_expires', 'timestamp', ['timezone' => true, 'null' => true])
            ->addColumn('permissions', 'text', ['null' => true])
            ->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['timezone' => true, 'null' => true])
            ->addIndex(['id'], ['unique' => true])
            ->addIndex(['id', 'token'], ['unique' => true])
            ->addIndex(['email'], ['unique' => true])
            ->create();

        $this->table('nrgsoft_auth_role', ['id' => false])
            ->addColumn('id', 'uuid')
            ->addColumn('name', 'string', ['limit' => 50])
            ->addColumn('description', 'string', ['limit' => 255])
            ->addColumn('permissions', 'text')
            ->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['timezone' => true, 'null' => true])
            ->addIndex(['id'], ['unique' => true])
            ->addIndex(['name'], ['unique' => true])
            ->create();

        $this->table('nrgsoft_auth_user_has_role', ['id' => false])
            ->addColumn('user_id', 'uuid')
            ->addColumn('role_id', 'uuid')
            ->addIndex(['user_id', 'role_id'], ['unique' => true])
            ->addForeignKey('user_id', 'nrgsoft_auth_user', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->addForeignKey('role_id', 'nrgsoft_auth_role', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->create();

        $this->table('nrgsoft_auth_token', ['id' => false])
            ->addColumn('id', 'string', ['limit' => 255])
            ->addColumn('user_id', 'uuid')
            ->addColumn('expires', 'timestamp', ['timezone' => true])
            ->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['id'], ['unique' => true])
            ->addForeignKey('user_id', 'nrgsoft_auth_user', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->create();
    }
}
