<?php

use Nrg\Files\Action\Directory\DeleteDirectoryAction;
use Nrg\Files\Action\Directory\CreateDirectoryAction;
use Nrg\Files\Action\Directory\ReadDirectoryAction;
use Nrg\Files\Action\File\CopyFileAction;
use Nrg\Files\Action\File\CreateFileAction;
use Nrg\Files\Action\File\DeleteFileAction;
use Nrg\Files\Action\File\DownloadFileAction;
use Nrg\Files\Action\File\IsFileExistsAction;
use Nrg\Files\Action\File\MoveFileAction;
use Nrg\Files\Action\File\OpenFileAction;
use Nrg\Files\Action\File\ReadFileAction;
use Nrg\Files\Action\File\UpdateFileAction;
use Nrg\Files\Action\File\UploadFileAction;
use Nrg\Files\Action\Hyperlink\CreateHyperlinkAction;
use Nrg\Files\Action\Storage\Ftp\CreateFtpStorageAction;
use Nrg\Files\Action\Storage\Ftp\UpdateFtpStorageAction;
use Nrg\Files\Action\Storage\Local\CreateLocalStorageAction;
use Nrg\Files\Action\Storage\Local\UpdateLocalStorageAction;
use Nrg\Files\Action\Storage\Sftp\CreateSftpStorageAction;
use Nrg\Files\Action\Storage\DeleteStorageAction;
use Nrg\Files\Action\Storage\Sftp\UpdateSftpStorageAction;
use Nrg\Files\Action\Storage\StorageDetailsAction;
use Nrg\Files\Action\Storage\StorageListAction;
use Nrg\Files\Persistence\Abstraction\FileRepository;
use Nrg\Files\Persistence\Abstraction\StorageRepository;
use Nrg\Files\Persistence\Factory\TempStorageFactory;
use Nrg\Files\Persistence\Repository\DbalStorageRepository;
use Nrg\Files\Persistence\Repository\FlysystemFileRepository;

return [
    'routes' => [
        '/file-manager/directory/read' => ReadDirectoryAction::class,
        '/file-manager/directory/create' => CreateDirectoryAction::class,
        '/file-manager/directory/delete' => DeleteDirectoryAction::class,

        '/file-manager/file/read' => ReadFileAction::class,
        '/file-manager/file/open' => OpenFileAction::class,
        '/file-manager/file/download' => DownloadFileAction::class,
        '/file-manager/file/create' => CreateFileAction::class,
        '/file-manager/file/update' => UpdateFileAction::class,
        '/file-manager/file/upload' => UploadFileAction::class,
        '/file-manager/file/copy' => CopyFileAction::class,
        '/file-manager/file/move' => MoveFileAction::class,
        '/file-manager/file/delete' => DeleteFileAction::class,
        '/file-manager/file/exists' => IsFileExistsAction::class,

        '/file-manager/hyperlink/create' => CreateHyperlinkAction::class,

        '/file-manager/storage/local/create' => CreateLocalStorageAction::class,
        '/file-manager/storage/local/update' => UpdateLocalStorageAction::class,
        '/file-manager/storage/ftp/create' => CreateFtpStorageAction::class,
        '/file-manager/storage/ftp/update' => UpdateFtpStorageAction::class,
        '/file-manager/storage/sftp/create' => CreateSftpStorageAction::class,
        '/file-manager/storage/sftp/update' => UpdateSftpStorageAction::class,
        '/file-manager/storage/list' => StorageListAction::class,
        '/file-manager/storage/details' => StorageDetailsAction::class,
        '/file-manager/storage/delete' => DeleteStorageAction::class,
    ],
    'services' => [
        FileRepository::class => FlysystemFileRepository::class,
        StorageRepository::class => DbalStorageRepository::class,
    ],
    'config' => [
        TempStorageFactory::class => [
            'root' => realpath(__DIR__.'/temp-storage'),
        ],
    ],
];
