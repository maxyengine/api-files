<?php

namespace Nrg\I18n\Service;

use Nrg\I18n\Abstraction\Translator;
use Nrg\I18n\Value\Message;

/**
 * Trait TranslatorAbility.
 */
trait TranslatorAbility
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * @param Message|string $message
     *
     * @return string
     */
    public function t($message): string
    {
        return null === $this->translator ? $message : $this->translator->translate($message);
    }

    /**
     * @param string $class
     */
    public function addDictionary(string $class): void
    {
        if (null !== $this->translator) {
            $this->translator->addDictionary($class);
        }
    }
}
