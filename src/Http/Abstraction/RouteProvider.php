<?php

namespace Nrg\Http\Abstraction;

use Nrg\Http\Value\Url;
use Nrg\Rx\Abstraction\Observer;
use ReflectionException;

/**
 * Interface RouteProvider.
 */
interface RouteProvider
{
    /**
     * Sets the action corresponding to the route.
     *
     * @param string $route
     * @param string $action
     *
     * @return RouteProvider
     */
    public function when(string $route, $action): self;

    /**
     * Sets the action by default.
     *
     * @param string $action
     *
     * @return RouteProvider
     */
    public function otherwise($action): self;

    /**
     * Returns the action corresponding to the request.
     *
     * @return Observer
     * @throws ReflectionException
     */
    public function getAction(): Observer;

    /**
     * @return array
     */
    public function getRoutePaths(): array;

    /**
     * @param string $path
     * @param array $params
     *
     * @return Url
     */
    public function createUrl(string $path, array $params = []): Url;
}
