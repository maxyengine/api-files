<?php

namespace Nrg\Http\UseCase;

use Nrg\Http\Value\HttpResponse;

/**
 * Class EmitResponse.
 *
 * Emits the HTTP response.
 */
class EmitResponse
{
    /**
     * Emits the HTTP response.
     *
     * @param HttpResponse $response
     * @param bool         $terminate
     */
    public function execute(HttpResponse $response, $terminate = false)
    {
        header(sprintf(
            'HTTP/%s %d %s',
            $response->getProtocolVersion(),
            $response->getStatus()->getCode(),
            $response->getStatus()->getReasonPhrase()
        ));

        foreach ($response->getHeaders() as $name => $values) {
            $name = str_replace(' ', '-', ucwords(str_replace('-', ' ', $name)));
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }

        echo $response->getBody();

        if ($terminate) {
            exit;
        }
    }
}
