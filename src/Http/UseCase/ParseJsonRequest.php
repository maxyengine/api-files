<?php

namespace Nrg\Http\UseCase;

use Nrg\Http\Value\HttpRequest;
use Nrg\Http\Value\HttpResponse;

class ParseJsonRequest
{
    /**
     * Handles JSON request.
     * Sets request body params from decoded request body.
     *
     * @param HttpRequest  $request
     * @param HttpResponse $response
     */
    public function execute(HttpRequest $request, HttpResponse $response)
    {
        //TODO : add checking json
        $bodyParams = json_decode($request->getBody(), true) ?? [];
        $request->setBodyParams($bodyParams + $request->getBodyParams());
        $response->setHeader('Content-Type', 'application/json; charset=utf-8');
    }
}
