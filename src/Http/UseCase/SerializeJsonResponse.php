<?php

namespace Nrg\Http\UseCase;

use Nrg\Http\Value\HttpResponse;

class SerializeJsonResponse
{
    public function execute(HttpResponse $response)
    {
        if ($response->containsInHeader('Content-Type', 'application/json')) {
            $response->setHeader('Content-Type', 'application/json; charset=utf-8');
            $response->setBody(
                json_encode($response->getBody(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            );
        }
    }
}
