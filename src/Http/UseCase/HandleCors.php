<?php

namespace Nrg\Http\UseCase;

use Nrg\Http\Value\HttpRequest;
use Nrg\Http\Value\HttpResponse;
use Nrg\Http\Value\HttpStatus;
use Nrg\Utility\Abstraction\Config;
use Nrg\Utility\Abstraction\Settings;

/**
 * Class HandleCors
 */
class HandleCors
{
    /**
     * @var EmitResponse
     */
    private $emitResponse;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param EmitResponse $emitResponse
     * @param Settings $settings
     */
    public function __construct(EmitResponse $emitResponse, Settings $settings)
    {
        $this->emitResponse = $emitResponse;
        $this->config = $settings->getConfig(static::class);
    }

    public function execute(HttpRequest $request, HttpResponse $response)
    {
        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // you want to allow, and if so:
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }

        /*$response->setHeaders($this->config->get('headers'));

        if ('OPTIONS' === $request->getMethod()) {
            $response->setStatus(new HttpStatus(HttpStatus::OK));
            $this->emitResponse->execute($response, true);
        }*/
    }
}
