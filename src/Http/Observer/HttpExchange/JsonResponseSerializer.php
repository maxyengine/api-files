<?php

namespace Nrg\Http\Observer\HttpExchange;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\UseCase\SerializeJsonResponse;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Throwable;

/**
 * Class JsonResponseSerializer
 */
class JsonResponseSerializer implements Observer
{
    use ObserverStub;

    /**
     * @var SerializeJsonResponse
     */
    private $serializeJsonResponse;

    /**
     * @param SerializeJsonResponse $serializeJsonResponse
     */
    public function __construct(SerializeJsonResponse $serializeJsonResponse)
    {
        $this->serializeJsonResponse = $serializeJsonResponse;
    }

    /**
     * Handles JSON response on ExchangeEvent process.
     *
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->serializeJsonResponse->execute($event->getResponse());
    }

    /**
     * @param Throwable $throwable
     * @param HttpExchangeEvent $event
     *
     * @throws Throwable
     */
    public function onError(Throwable $throwable, $event)
    {
        $this->serializeJsonResponse->execute($event->getResponse());
    }
}
