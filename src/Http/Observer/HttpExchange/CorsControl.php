<?php

namespace Nrg\Http\Observer\HttpExchange;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\UseCase\HandleCors;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

class CorsControl implements Observer
{
    use ObserverStub;

    /**
     * @var HandleCors
     */
    private $handleCORS;

    public function __construct(HandleCors $handleCORS)
    {
        $this->handleCORS = $handleCORS;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->handleCORS->execute($event->getRequest(), $event->getResponse());
    }
}
