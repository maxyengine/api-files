<?php

namespace Nrg\Http\Observer\HttpExchange;

use Nrg\Http\Abstraction\RouteProvider;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use ReflectionException;
use Throwable;

/**
 * Class RouteHandler.
 *
 * Manages the launch of the corresponding routing action when the HttpExchangeEvent are triggered by observable.
 * The action should implement Observer interface.
 */
class Controller implements Observer
{
    use ObserverStub;

    /**
     * @var RouteProvider
     */
    private $routeProvider;

    /**
     * @var null|Observer
     */
    private $action;

    public function __construct(RouteProvider $routeProvider)
    {
        $this->routeProvider = $routeProvider;
    }

    /**
     * Runs a corresponding routing action when the HttpExchangeEvent are triggered by observable.
     * The action should implement Observer interface.
     *
     * @param HttpExchangeEvent $event
     *
     * @throws ReflectionException
     */
    public function onNext($event)
    {
        $this->action = $this->routeProvider->getAction();
        $this->action->onNext($event);
    }

    /**
     * Runs error handler on corresponding routing action.
     *
     * @param Throwable $throwable
     * @param HttpExchangeEvent $event
     *
     * @throws Throwable
     */
    public function onError(Throwable $throwable, $event)
    {
        if (null !== $this->action) {
            $this->action->onError($throwable, $event);
        }
    }

    /**
     * Runs completion handler on corresponding routing action.
     */
    public function onComplete()
    {
        if (null !== $this->action) {
            $this->action->onComplete();
        }
    }
}
