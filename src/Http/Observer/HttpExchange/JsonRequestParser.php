<?php

namespace Nrg\Http\Observer\HttpExchange;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\UseCase\ParseJsonRequest;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class JsonRequestParser
 */
class JsonRequestParser implements Observer
{
    use ObserverStub;

    /**
     * @var ParseJsonRequest
     */
    private $parseJSONRequest;

    /**
     * JSONRequestHandler constructor.
     */
    public function __construct()
    {
        $this->parseJSONRequest = new ParseJsonRequest();
    }

    /**
     * Handles JSON request on ExchangeEvent process.
     *
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->parseJSONRequest->execute($event->getRequest(), $event->getResponse());
    }
}
