<?php

namespace Nrg\Http\Observer\HttpExchange;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Lang\Service\ErrorToExceptionTranslator;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class ExceptionHandler
 */
class ExceptionHandler implements Observer
{
    use ObserverStub;

    /**
     * @var ErrorToExceptionTranslator
     */
    private $errorToExceptionTranslator;

    /**
     * ExceptionHandler constructor.
     */
    public function __construct()
    {
        $this->errorToExceptionTranslator = new ErrorToExceptionTranslator();
    }

    /**
     * Handles JSON request on ExchangeEvent process.
     *
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->errorToExceptionTranslator->enable();
    }
}
