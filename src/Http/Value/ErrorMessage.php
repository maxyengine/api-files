<?php

namespace Nrg\Http\Value;

use JsonSerializable;
use Throwable;

/**
 * Class ErrorMessage
 */
class ErrorMessage implements JsonSerializable
{
    /**
     * @var Throwable
     */
    private $throwable;

    /**
     * @param Throwable $throwable
     */
    public function __construct(Throwable $throwable)
    {
        $this->throwable = $throwable;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        return [
            'message' => $this->throwable->getMessage(),
        ];
    }
}