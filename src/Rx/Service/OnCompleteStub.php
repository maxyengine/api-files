<?php

namespace Nrg\Rx\Service;

/**
 * Trait ObserverStub.
 *
 * Facilitates the implementation of the observer.
 */
trait OnCompleteStub
{
    public function onComplete()
    {
    }
}
