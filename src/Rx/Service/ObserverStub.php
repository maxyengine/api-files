<?php

namespace Nrg\Rx\Service;

use Throwable;

/**
 * Trait ObserverStub.
 *
 * Facilitates the implementation of the observer.
 */
trait ObserverStub
{
    /**
     * @param Throwable $throwable
     * @param $event
     */
    public function onError(Throwable $throwable, $event)
    {
    }

    public function onComplete()
    {
    }
}
