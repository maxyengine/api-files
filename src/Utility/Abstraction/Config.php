<?php

namespace Nrg\Utility\Abstraction;

/**
 * Interface Config.
 */
interface Config
{
    /**
     * @param string $key
     * @param null $defaultValue
     *
     * @return mixed
     */
    public function get(string $key, $defaultValue = null);

    /**
     * @return array
     */
    public function asArray(): array;
}
