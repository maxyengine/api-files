<?php

namespace Nrg\Utility\Service;

use DomainException;
use Nrg\Data\Exception\EntityNotFoundException;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\ErrorMessage;
use Nrg\Http\Value\HttpStatus;
use RuntimeException;
use Throwable;

/**
 * Trait OnErrorResponse
 */
trait OnErrorResponse
{
    /**
     * @param Throwable $throwable
     * @param HttpExchangeEvent $event
     *
     * @throws Throwable
     */
    public function onError(Throwable $throwable, $event)
    {
        $event->getResponse()->setBody(new ErrorMessage($throwable));
        try {
            throw $throwable;
        } catch (EntityNotFoundException $e) {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::NOT_FOUND));
        } catch (DomainException $e) {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::BAD_REQUEST));
        } catch (RuntimeException $e) {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::INTERNAL_SERVER_ERROR));
        }
    }
}