<?php

namespace Nrg\Files\Persistence\Repository;

use Aws\S3\S3Client;
use DateTime;
use League\Flysystem\Adapter\Ftp;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Directory as FlysystemDirectory;
use League\Flysystem\FileExistsException;
use League\Flysystem\Filesystem;
use League\Flysystem\MountManager;
use League\Flysystem\RootViolationException;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;
use Nrg\Files\Entity\Directory;
use Nrg\Files\Entity\File;
use Nrg\Files\Entity\Hyperlink;
use Nrg\Files\Entity\Storage;
use Nrg\Files\Entity\Storage\LocalStorage;
use Nrg\Files\Entity\Storage\ZipStorage;
use Nrg\Files\Persistence\Abstraction\FileRepository;
use Nrg\Files\Persistence\Abstraction\StorageRepository;
use Nrg\Files\Persistence\Factory\TempStorageFactory;
use Nrg\Files\Value\Path;
use Nrg\Files\Value\Permissions;
use Nrg\Files\Value\Size;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;
use Nrg\Utility\Abstraction\Settings;
use RuntimeException;

/**
 * Class FlysystemFileRepository.
 *
 * Flysystem file repository implementation.
 *
 * @see https://github.com/thephpleague/flysystem
 */
class FlysystemFileRepository implements FileRepository
{
    /**
     * @var MountManager
     */
    private $mountManager;

    /**
     * @var StorageRepository
     */
    private $storageRepository;

    /**
     * @var TempStorageFactory
     */
    private $tempStorageFactory;

    /**
     * @param StorageRepository $storageRepository
     * @param TempStorageFactory $tempStorageFactory
     */
    public function __construct(StorageRepository $storageRepository, TempStorageFactory $tempStorageFactory)
    {
        $this->mountManager = new MountManager();
        $this->storageRepository = $storageRepository;
        $this->tempStorageFactory = $tempStorageFactory;
    }

///////////////////////todo: remove

    /**
     * IOFileAdapter constructor.
     *
     * @param Settings $settings
     */
    public function __construct_old(Settings $settings)
    {
        //////////////////////

//        $isLocal = false;
        $isLocal = true;

        if ($isLocal) {

            $rootPath = $settings->getConfig(FileRepository::class)->get('rootPath');
            $this->mountManager = new Filesystem(new Local($rootPath));

        } else {
            // FTP
//            $this->filesystem = new Filesystem(new Ftp($settings->getConfig(FileRepository::class)->get('FTP')));

            // AWS S3
            $adapter = new AwsS3Adapter(
                S3Client::factory(
                    $rootPath = $settings->getConfig(FileRepository::class)->get('AWS S3')
                ), 'realtime-fun'
            );
            $this->mountManager = new Filesystem($adapter);
        }
    }

///////////////////////

    public function has(Path $path): bool
    {
        $this->mountStorage($path->getStorageId());

        return $path->isRoot() || $this->mountManager->has((string)$path);
    }

    /**
     * {@inheritdoc}
     */
    public function readDirectory(Path $path): Directory
    {
        $this->mountStorage($path->getStorageId());

        return $this->createEntity($path->getStorageId(), $this->getMetadata($path));
    }

    /**
     * {@inheritdoc}
     */
    public function createDirectory(Directory $directory): void
    {
        $this->mountStorage($directory->getPath()->getStorageId());

        $result = $this->mountManager->createDir((string)$directory->getPath());

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during creating the directory \'%s\'', $directory->getPath())
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function readFile(Path $path): File
    {
        $this->mountStorage($path->getStorageId());
        $contents = $this->mountManager->read((string)$path);

        return $this
            ->createEntity($path->getStorageId(), $this->getMetadata($path))
            ->setContents($contents);
    }

    /**
     * {@inheritdoc}
     */
    public function createFile(File $file): void
    {
        $this->mountStorage($file->getPath()->getStorageId());

        $result = $this->mountManager->write((string)$file->getPath(), $file->getContents());

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during creating the file \'%s\'', $file->getPath())
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateFile(File $file): void
    {
        $this->mountStorage($file->getPath()->getStorageId());

        $result = $this->mountManager->update((string)$file->getPath(), $file->getContents());

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during updating the file \'%s\'', $file->getPath())
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function copyFile(Path $path, Path $newPath): void
    {
        $this
            ->mountStorage($path->getStorageId())
            ->mountStorage($newPath->getStorageId());

        try {
            $result = $this->mountManager->copy((string)$path, (string)$newPath);
        } catch (FileExistsException $e) {
            throw new RuntimeException($e->getMessage());
        }

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during copying the file \'%s\'', $path)
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function moveFile(Path $path, Path $newPath): void
    {
        $this
            ->mountStorage($path->getStorageId())
            ->mountStorage($newPath->getStorageId());

        $result = $this->mountManager->move((string)$path, (string)$newPath);

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during moving the file \'%s\'', $path)
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function writeStream(File $file, $stream): void
    {
        $this->mountStorage($file->getPath()->getStorageId());

        $result = $this->mountManager->writeStream((string)$file->getPath(), $stream);

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during uploading the file \'%s\'', $file->getPath())
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteFile(Path $path): void
    {
        $this->mountStorage($path->getStorageId());

        $result = $this->mountManager->delete((string)$path);

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during deleting the file \'%s\'', $path)
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteDirectory(Path $path): void
    {
        $this->mountStorage($path->getStorageId());

        try {
            $result = $this->mountManager->deleteDir((string)$path);
        } catch (RootViolationException $e) {
            throw new RuntimeException($e->getMessage());
        }

        if (false === $result) {
            throw new RuntimeException(
                sprintf('Error occurred during deleting the directory \'%s\'', $path)
            );
        }
    }

    /**
     * @param Path $path
     *
     * @return array
     */
    private function getMetadata(Path $path): array
    {
        if ($path->isRoot()) {
            return [
                'type' => File::TYPE_DIRECTORY,
                'path' => $path->getFilePath(),
                'children' => $this->mountManager->listContents((string)$path),
            ];
        }

        $file = $this->mountManager->get((string)$path);

        if ($file instanceof FlysystemDirectory) {
            return [
                'type' => File::TYPE_DIRECTORY,
                'path' => $path->getFilePath(),
                'children' => $file->getContents(),
            ];
        } else {
            return [
                    'path' => $path->getFilePath(),
                    'type' => $this->determineEntityType($file->getMetadata()),
                    'mimeType' => $file->getMimetype(),
                ] + $file->getMetadata();
        }
    }

    /**
     * @param array $file
     *
     * @return File|Directory
     */
    private function createEntity(string $storageId, array $file): File
    {
        switch ($file['type']) {
            case File::TYPE_DIRECTORY:
                $entity = new Directory(Path::create($storageId, $file['path'], true));

                if (isset($file['children'])) {
                    $entity->setChildren();
                    foreach ($file['children'] as $child) {
                        $child['type'] = $this->determineEntityType($child);
                        $entity->addChild($this->createEntity($storageId, $child));
                    }
                }
                break;
            case File::TYPE_HYPERLINK:
                $entity = new Hyperlink(Path::create($storageId, $file['path']));
                break;
            default:
                $entity = new File(Path::create($storageId, $file['path']));
        }

        if (isset($file['type'])) {
            $entity->setType($file['type']);
        }

        if (isset($file['mimeType'])) {
            $entity->setMimeType($file['mimeType']);
        }

        if (isset($file['size'])) {
            $entity->setSize(new Size((int)$file['size']));
        }

        if (isset($file['permissions'])) {
            $entity->setPermissions(new Permissions($file['permissions']));
        }

        if (isset($file['timestamp'])) {
            $entity->setLastModified(new DateTime(date('Y-m-d H:i:s', $file['timestamp'])));
        }

        return $entity;
    }

    private function determineEntityType($raw)
    {
        if ('dir' === $raw['type']) {
            return File::TYPE_DIRECTORY;
        }

        $extension = $raw['extension'] ?? pathinfo($raw['path'], PATHINFO_EXTENSION);

        if (in_array($extension, ['https', 'http'])) {
            return File::TYPE_HYPERLINK;
        }

        return File::TYPE_FILE;
    }

    /**
     * @param Path $path
     *
     * @return FlysystemFileRepository
     */
    private function mountStorage(string $storageId): FlysystemFileRepository
    {
        if (Storage::isTemporary($storageId)) {
            $storage = $this->tempStorageFactory->createById($storageId);
        } else {
            $storage = $this->storageRepository->findOne(
                (new Filter())
                    ->addCondition(
                        (new Equal())
                            ->setValue($storageId)
                            ->setField('id')
                    )
            );
        }

        /**
         * @var $storage LocalStorage|ZipStorage
         */
        switch ($storage->getType()) {
            case Storage::TYPE_LOCAL:
                $filesystem = new Filesystem(new Local($storage->getRoot()));
                break;
            case Storage::TYPE_ZIP:
                $filesystem = new Filesystem(new ZipArchiveAdapter($storage->getLocation()));
                break;
            case Storage::TYPE_FTP:
                $filesystem = new Filesystem(new Ftp($storage->getParams()));
                break;
            case Storage::TYPE_SFTP:
                $filesystem = new Filesystem(new SftpAdapter($storage->getParams()));
                break;
        }

        $this->mountManager->mountFilesystem($storage->getId(), $filesystem);

        return $this;
    }
}
