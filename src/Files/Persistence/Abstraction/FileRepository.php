<?php

namespace Nrg\Files\Persistence\Abstraction;

use Nrg\Files\Entity\Directory;
use Nrg\Files\Entity\File;
use Nrg\Files\Value\Path;

/**
 * Interface FileRepository.
 */
interface FileRepository
{
    /**
     * Creates a new directory.
     *
     * @param Directory $directory
     */
    public function createDirectory(Directory $directory): void;

    /**
     * Returns a directory by a path.
     *
     * @param Path $path
     *
     * @return Directory
     */
    public function readDirectory(Path $path): Directory;

    /**
     * @param Path $path
     */
    public function deleteDirectory(Path $path): void;

    /**
     * Creates a new file.
     *
     * @param File $file
     */
    public function createFile(File $file): void;

    /**
     * Returns a file by a path.
     *
     * @param Path $path
     *
     * @return File
     */
    public function readFile(Path $path): File;

    /**
     * Updates a file content.
     *
     * @param File $file
     */
    public function updateFile(File $file): void;

    /**
     * @param Path $path
     */
    public function deleteFile(Path $path): void;

    /**
     * @param Path $path
     * @param Path $newPath
     */
    public function copyFile(Path $path, Path $newPath): void;

    /**
     * @param Path $path
     * @param Path $newPath
     */
    public function moveFile(Path $path, Path $newPath): void;

    /**
     * @param $path
     * @param $stream
     *
     * @return bool
     */
    public function writeStream(File $file, $stream): void;

    /**
     * @param Path $path
     *
     * @return bool
     */
    public function has(Path $path): bool;
}
