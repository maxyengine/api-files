<?php

namespace Nrg\Files\UseCase\File;

use DateTime;
use Nrg\Files\Persistence\Abstraction\FileRepository;
use Nrg\Files\Value\Path;
use Nrg\Files\Entity\File;
use Nrg\Files\Value\Permissions;
use Nrg\Files\Value\Size;
use Nrg\Data\Exception\EntityNotFoundException;

/**
 * Class UploadFile.
 *
 * Service to upload file.
 */
class UploadFile
{
    /**
     * @var FileRepository
     */
    private $repository;


    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }


    public function execute(array $data): File
    {
        $path = new Path($data['path'], true);
        $uploadedFile = $data['file'];

        if (!$this->repository->has($path)) {
            throw new EntityNotFoundException(sprintf('Directory \'%s\' is not exists or it\'s not readable', $path));
        }

        $file = (new File($path->join($uploadedFile->getName())))
            ->setSize(new Size($uploadedFile->getSize()))
            ->setType($uploadedFile->getType())
            ->setPermissions(new Permissions(0755))
            ->setLastModified(new DateTime());

        $stream = fopen($uploadedFile->getTmpName(), 'r+');

        $this->repository->writeStream($file, $stream);

        return $file;
    }
}
