<?php

namespace Nrg\Auth\Form\Role;

use Nrg\Auth\Form\Role\Element\Description;
use Nrg\Auth\Form\Role\Element\Name;
use Nrg\Auth\Form\Role\Element\Permissions;
use Nrg\Auth\Form\Role\Validator\UniqueName;
use Nrg\Auth\I18n\AuthDictionary;
use Nrg\Auth\UseCase\Role\IsUniqueName;
use Nrg\Form\Form;
use Nrg\I18n\Abstraction\Translator;

class CreateRoleForm extends Form
{
    /**
     * CreateRoleForm constructor.
     *
     * @param Translator   $translator
     * @param IsUniqueName $isUniqueName
     */
    public function __construct(Translator $translator, IsUniqueName $isUniqueName)
    {
        parent::__construct($translator);
        $translator->addDictionary(AuthDictionary::class);

        $this
            ->addElement((new Name())
                ->addValidator(new UniqueName($isUniqueName))
            )
            ->addElement(new Description())
            ->addElement(new Permissions());
    }
}
