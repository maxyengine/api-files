<?php

namespace Nrg\Auth\Form\Role\Element;

use Nrg\Form\Element;
use Nrg\Form\Filter\TrimFilter;
use Nrg\Form\Validator\LengthValidator;
use Nrg\Form\Validator\IsStringValidator;


class Description extends Element
{
    public function __construct()
    {
        parent::__construct('description');
        $this
            ->addFilter(new TrimFilter())
            ->addValidator(new IsStringValidator())
            ->addValidator(
                (new LengthValidator())
                    ->setMin(3)
                    ->setMax(255)
            );
    }
}
