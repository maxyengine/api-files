<?php

namespace Nrg\Auth\Form\Role\Element;

use Nrg\Auth\Form\Role\Validator\Permission;
use Nrg\Form\Element;
use Nrg\Form\Filter\TrimFilter;
use Nrg\Form\Validator\IsArrayValidator;

class Permissions extends Element
{
    public function __construct()
    {
        parent::__construct('permissions');

        $this->addValidator(
            (new IsArrayValidator())
                ->addValueFilter(new TrimFilter())
                ->addValueValidator(new Permission())
        );
    }
}
