<?php

namespace Nrg\Auth\Form\Role\Validator;

use Nrg\Form\Abstraction\AbstractValidator;
use Nrg\Form\Element;

/**
 * Class Permissions.
 */
class Permission extends AbstractValidator
{
    private const PATTERN = '/^(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?$/i';

    public function __construct()
    {
        $this->adjustErrorText('please provide a valid permission');
    }

    /**
     * @return bool
     */
    public function isValid(Element $element): bool
    {
        return 1 === preg_match(self::PATTERN, $element->getValue());
    }
}
