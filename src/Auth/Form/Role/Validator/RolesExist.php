<?php

namespace Nrg\Auth\Form\Role\Validator;

use Nrg\Auth\UseCase\Role\AreRolesExist;
use Nrg\Form\Abstraction\AbstractValidator;
use Nrg\Form\Element;

/**
 * Class RolesExist
 */
class RolesExist extends AbstractValidator
{
    public const CASE_CONTAINS_NON_EXISTENT_ROLE = 0;

    /**
     * @var AreRolesExist
     */
    private $areRolesExist;

    /**
     * @param AreRolesExist $areRolesExist
     */
    public function __construct(AreRolesExist $areRolesExist)
    {
        $this->adjustErrorText('List contains a non-existent role', self::CASE_CONTAINS_NON_EXISTENT_ROLE);
        $this->areRolesExist = $areRolesExist;
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function isValid(Element $element): bool
    {
        $this->setErrorCase(self::CASE_CONTAINS_NON_EXISTENT_ROLE);

        return $this->areRolesExist->execute([
            'roleIds' => $element->getValue()
        ]);
    }
}
