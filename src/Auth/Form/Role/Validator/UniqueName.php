<?php

namespace Nrg\Auth\Form\Role\Validator;

use Nrg\Auth\UseCase\Role\IsUniqueName;
use Nrg\Form\Abstraction\AbstractValidator;
use Nrg\Form\Element;

/**
 * Class UniqueName
 */
class UniqueName extends AbstractValidator
{
    public const CASE_ALREADY_EXISTS = 0;

    /**
     * @var string
     */
    private $exceptId;

    /**
     * @var IsUniqueName
     */
    private $isUniqueName;

    public function __construct(IsUniqueName $isUniqueName)
    {
        $this->adjustErrorText('role with name \'%s\' already exists', self::CASE_ALREADY_EXISTS);
        $this->isUniqueName = $isUniqueName;
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function isValid(Element $element): bool
    {
        $this->setErrorCase(self::CASE_ALREADY_EXISTS, $element->getValue());

        return $this->isUniqueName->execute([
            'name' => $element->getValue(),
            'exceptId' => $this->exceptId,
        ]);
    }

    /**
     * @param string $exceptId
     *
     * @return UniqueName
     */
    public function setExceptId(string $exceptId): self
    {
        $this->exceptId = $exceptId;

        return $this;
    }
}
