<?php

namespace Nrg\Auth\Form\Role\Details;

use Nrg\Auth\Form\Role\Element\Description;
use Nrg\Auth\Form\Role\Element\Name;
use Nrg\Auth\Form\Role\Validator\UniqueName;
use Nrg\Auth\I18n\AuthDictionary;
use Nrg\Auth\UseCase\Role\IsUniqueName;
use Nrg\Form\Element\UuidElement;
use Nrg\Form\Form;
use Nrg\I18n\Abstraction\Translator;

/**
 * Class UpdateRoleDetailsForm.
 */
class UpdateRoleDetailsForm extends Form
{
    /**
     * @var IsUniqueName
     */
    private $isUniqueName;

    /**
     * @param Translator   $translator
     * @param IsUniqueName $isUniqueName
     */
    public function __construct(Translator $translator, IsUniqueName $isUniqueName)
    {
        parent::__construct($translator);
        $translator->addDictionary(AuthDictionary::class);

        $this->isUniqueName = $isUniqueName;

        $this
            ->addElement(new UuidElement())
            ->addElement(new Name())
            ->addElement(new Description());
    }

    /**
     * {@inheritdoc}
     */
    public function populate(array $data): Form
    {
        parent::populate($data);
        $id = $this->getElement('id');
        if (!$id->hasError()) {
            $this->getElement('name')
                ->addValidator(
                    (new UniqueName($this->isUniqueName))
                        ->setExceptId($id->getValue())
                );
        }

        return $this;
    }
}
