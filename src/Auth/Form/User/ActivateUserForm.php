<?php

namespace Nrg\Auth\Form\User;

use Nrg\Auth\Form\User\Element\Password;
use Nrg\Auth\Form\User\Element\Token;
use Nrg\Auth\I18n\AuthDictionary;
use Nrg\Auth\UseCase\User\IsUserTokenExist;
use Nrg\Form\Element\UuidElement;
use Nrg\Form\Form;
use Nrg\I18n\Abstraction\Translator;

class ActivateUserForm extends Form
{
    public function __construct(Translator $translator, IsUserTokenExist $isUserTokenExist)
    {
        parent::__construct($translator);

        $translator->addDictionary(AuthDictionary::class);

        $this
            ->addElement(new UuidElement())
            ->addElement(new Token($isUserTokenExist))
            ->addElement(new Password());
    }
}
