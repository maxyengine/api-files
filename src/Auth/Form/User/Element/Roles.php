<?php

namespace Nrg\Auth\Form\User\Element;

use Nrg\Auth\Form\Role\Validator\RolesExist;
use Nrg\Auth\UseCase\Role\AreRolesExist;
use Nrg\Form\Element;
use Nrg\Form\Filter\TrimFilter;
use Nrg\Form\Validator\IsArrayValidator;
use Nrg\Form\Validator\IsUuidValidator;

/**
 * Class Roles
 */
class Roles extends Element
{
    public function __construct(AreRolesExist $areRolesExistUser)
    {
        parent::__construct('roles');

        $this
            ->addValidator(
                (new IsArrayValidator())
                    ->addValueFilter(new TrimFilter())
                    ->addValueValidator(new IsUuidValidator())
            )
            ->addValidator(new RolesExist($areRolesExistUser))
        ;
    }
}
