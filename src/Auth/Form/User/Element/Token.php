<?php

namespace Nrg\Auth\Form\User\Element;

use Nrg\Auth\Form\User\Validator\UserTokenExists;
use Nrg\Auth\UseCase\User\IsUserTokenExist;
use Nrg\Form\Element;
use Nrg\Form\Filter\TrimFilter;
use Nrg\Form\Validator\LengthValidator;
use Nrg\Form\Validator\IsStringValidator;
use Nrg\Form\Validator\IsRequiredValidator;

/**
 * Class Token
 */
class Token extends Element
{
    public function __construct(IsUserTokenExist $isUserTokenExist)
    {
        parent::__construct('token');

        $this
            ->addFilter(new TrimFilter())
            ->addValidator(new IsRequiredValidator())
            ->addValidator(new IsStringValidator())
            ->addValidator(
                (new LengthValidator())
                    ->setMin(32)
                    ->setMax(255)
            )
            ->addValidator(new UserTokenExists($isUserTokenExist))
        ;
    }
}
