<?php

namespace Nrg\Auth\Form\User\Element;

use Nrg\Form\Element;
use Nrg\Form\Filter\TrimFilter;
use Nrg\Form\Validator\LengthValidator;
use Nrg\Form\Validator\IsStringValidator;

/**
 * Class Name.
 */
class Name extends Element
{
    public function __construct()
    {
        parent::__construct('name');
        $this
            ->addFilter(new TrimFilter())
            ->addValidator(new IsStringValidator())
            ->addValidator(
                (new LengthValidator())
                    ->setMin(1)
                    ->setMax(50)
            );
    }
}
