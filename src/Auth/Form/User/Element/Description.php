<?php

namespace Nrg\Auth\Form\User\Element;

use Nrg\Form\Element;
use Nrg\Form\Filter\TrimFilter;
use Nrg\Form\Validator\LengthValidator;
use Nrg\Form\Validator\IsStringValidator;

/**
 * Class Description.
 */
class Description extends Element
{
    public function __construct()
    {
        parent::__construct('description');
        $this
            ->addFilter(new TrimFilter())
            ->addValidator(new IsStringValidator())
            ->addValidator(
                (new LengthValidator())
                    ->setMax(255)
            );
    }
}
