<?php

namespace Nrg\Auth\Form\User\Validator;

use Nrg\Auth\UseCase\User\IsUniqueEmail;
use Nrg\Form\Abstraction\AbstractValidator;
use Nrg\Form\Element;

/**
 * Class UniqueEmail
 */
class UniqueEmail extends AbstractValidator
{
    public const CASE_ALREADY_EXISTS = 0;

    /**
     * @var string
     */
    private $exceptId;

    /**
     * @var IsUniqueEmail
     */
    private $isUniqueEmail;

    /**
     * @param IsUniqueEmail $isUniqueEmail
     */
    public function __construct(IsUniqueEmail $isUniqueEmail)
    {
        $this->adjustErrorText('user with email \'%s\' already exists', self::CASE_ALREADY_EXISTS);
        $this->isUniqueEmail = $isUniqueEmail;
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function isValid(Element $element): bool
    {
        $this->setErrorCase(self::CASE_ALREADY_EXISTS, $element->getValue());

        return $this->isUniqueEmail->execute([
            'email' => $element->getValue(),
            'exceptId' => $this->exceptId,
        ]);
    }

    /**
     * @param string $exceptId
     *
     * @return UniqueEmail
     */
    public function setExceptId(string $exceptId): self
    {
        $this->exceptId = $exceptId;

        return $this;
    }
}
