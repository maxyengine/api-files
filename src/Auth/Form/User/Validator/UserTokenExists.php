<?php

namespace Nrg\Auth\Form\User\Validator;

use Nrg\Auth\UseCase\User\IsTokenExist;
use Nrg\Form\Abstraction\AbstractValidator;
use Nrg\Form\Element;

/**
 * Class UserTokenExists
 */
class UserTokenExists extends AbstractValidator
{
    public const CASE_TOKEN_IS_NOT_EXISTS = 0;

    /**
     * @var IsTokenExist
     */
    private $userTokenExist;

    /**
     * @param IsTokenExist $isUserTokenExist
     */
    public function __construct(IsTokenExist $isUserTokenExist)
    {
        $this->userTokenExist = $isUserTokenExist;
        $this->adjustErrorText('token \'%s\' is not exist', self::CASE_TOKEN_IS_NOT_EXISTS);
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function isValid(Element $element): bool
    {
        $this->setErrorCase(self::CASE_TOKEN_IS_NOT_EXISTS, $element->getValue());

        return $this->userTokenExist->execute([
            'token' => $element->getValue(),
            'id' => $element->getForm()->getElement('id')->getValue(),
        ]);
    }
}
