<?php

namespace Nrg\Auth\Form\User;

use Nrg\Auth\Form\User\Element\Description;
use Nrg\Auth\Form\User\Element\EmailElement;
use Nrg\Auth\Form\User\Element\Name;
use Nrg\Auth\Form\User\Element\Roles;
use Nrg\Auth\Form\User\Validator\UniqueEmail;
use Nrg\Auth\I18n\AuthDictionary;
use Nrg\Auth\UseCase\Role\AreRolesExist;
use Nrg\Auth\UseCase\User\IsUniqueEmail;
use Nrg\Form\Form;
use Nrg\I18n\Abstraction\Translator;

class CreateUserForm extends Form
{
    /**
     * @param Translator    $translator
     * @param IsUniqueEmail $isUserUniqueEmail
     * @param AreRolesExist $areRolesExist
     */
    public function __construct(
        Translator $translator,
        IsUniqueEmail $isUserUniqueEmail,
        AreRolesExist $areRolesExist
    ) {
        parent::__construct($translator);
        $translator->addDictionary(AuthDictionary::class);

        $this
            ->addElement(
                (new EmailElement())
                    ->addValidator(new UniqueEmail($isUserUniqueEmail))
            )
            ->addElement(new Name())
            ->addElement(new Roles($areRolesExist))
            ->addElement(new Description());
    }
}
