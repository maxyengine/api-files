<?php

namespace Nrg\Auth\Value;

use InvalidArgumentException;

/**
 * Class UserStatus
 */
class UserStatus
{
    public const PENDING = 'pending';
    public const ACTIVE = 'active';

    /**
     * @var array
     */
    private $allowed = [
        self::PENDING,
        self::ACTIVE,
    ];

    /**
     * @var string
     */
    private $value;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        if (!in_array($value, $this->allowed)) {
            throw new InvalidArgumentException('Invalid user status was provided');
        }

        $this->value = $value;
    }

    public static function createPending(): UserStatus
    {
        return new self(self::PENDING);
    }

    public static function createActive(): UserStatus
    {
        return new self(self::ACTIVE);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->getValue();
    }
}
