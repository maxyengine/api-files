<?php

namespace Nrg\Auth\Value;

use JsonSerializable;
use Nrg\Http\Value\Url;

class Permissions implements JsonSerializable
{
    /**
     * @var Url[]
     */
    private $urls = [];

    /**
     * @param Url $url
     *
     * @return Permissions
     */
    public function addUrl(Url $url): Permissions
    {
        $this->urls[] = $url;

        return $this;
    }

    /**
     * @param Url $verify
     *
     * @return bool
     */
    public function isAllowed(Url $verify): bool
    {
        foreach ($this->urls as $url) {
            if ($verify->contains($url)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        $array = [];
        foreach ($this->urls as $url) {
            $array[] = $url->getPath();
        }

        return $array;
    }
}
