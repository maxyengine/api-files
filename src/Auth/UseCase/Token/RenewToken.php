<?php

namespace Nrg\Auth\UseCase\Token;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Exception;
use Nrg\Auth\Abstraction\AccessTokenGenerator;
use Nrg\Auth\Entity\Token;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;

/**
 * Class RenewToken
 */
class RenewToken
{
    /**
     * @var TokenRepository
     */
    private $repository;

    /**
     * @var AccessTokenGenerator
     */
    private $tokenGenerator;

    /**
     * @param TokenRepository $repository
     */
    public function __construct(TokenRepository $repository, AccessTokenGenerator $tokenGenerator)
    {
        $this->repository = $repository;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param array $data
     *
     * @return Token
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function execute(Token $token): Token
    {
        $this->repository->delete($token);

        $newToken = $this->tokenGenerator->generate($token->getUser());

        $this->repository->create($newToken);

        return $newToken;
    }
}
