<?php

namespace Nrg\Auth\UseCase\Token;

use Nrg\Auth\Entity\Token;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

class DetailsToken
{
    /**
     * @var TokenRepository
     */
    private $repository;

    /**
     * DetailsToken constructor.
     *
     * @param TokenRepository $repository
     */
    public function __construct(TokenRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return Token
     */
    public function execute(array $data): Token
    {
        return $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));
    }
}
