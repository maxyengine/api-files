<?php

namespace Nrg\Auth\UseCase\Token;

use DateTime;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Condition\Greater;
use Nrg\Data\Dto\Filter;

/**
 * Class IsTokenExist
 */
class IsTokenExist
{
    /**
     * @var TokenRepository
     */
    private $tokenRepository;

    /**
     * @param TokenRepository $tokenRepository
     */
    public function __construct(TokenRepository $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    public function execute(array $data): bool
    {
        $filter = (new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            )
            ->addCondition(
                (new Greater())
                    ->setValue((new DateTime())->format('Y-m-d H:i:s'))
                    ->setField('expires')
            )
        ;

        return $this->tokenRepository->exists($filter);
    }
}