<?php

namespace Nrg\Auth\UseCase\Role;

use DateTime;
use Nrg\Auth\Entity\Role;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Auth\Value\Permissions;
use Nrg\Utility\Abstraction\Uuid;

/**
 * Class CreateRole.
 */
class CreateRole
{
    /**
     * @var RoleRepository
     */
    private $repository;

    /**
     * @var Uuid
     */
    private $uuid;

    /**
     * SaveRole constructor.
     *
     * @param RoleRepository $repository
     */
    public function __construct(RoleRepository $repository, Uuid $uuid)
    {
        $this->repository = $repository;
        $this->uuid = $uuid;
    }

    /**
     * @param array $data
     *
     * @return Role
     */
    public function execute(array $data): Role
    {
        $role = new Role($this->uuid->generateV4());

        $data['createdAt'] = new DateTime();
        $data['permissions'] = new Permissions(...$data['permissions']);
        $role->populateObject($data);

        $this->repository->create($role);

        return $role;
    }
}
