<?php

namespace Nrg\Auth\UseCase\Role\Details;

use DateTime;
use Nrg\Auth\Entity\Role;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

class UpdateRoleDetails
{
    /**
     * @var RoleRepository
     */
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(array $data): Role
    {
        $role = $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));

        if ($role->populateEntity($data)) {
            $role->populateObject(['updatedAt' => new DateTime()]);
            $this->repository->update($role);
        }

        return $role;
    }
}
