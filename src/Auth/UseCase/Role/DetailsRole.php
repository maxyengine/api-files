<?php

namespace Nrg\Auth\UseCase\Role;

use Nrg\Auth\Entity\Role;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

class DetailsRole
{
    /**
     * @var RoleRepository
     */
    private $repository;

    /**
     * DetailsRole constructor.
     *
     * @param RoleRepository $repository
     */
    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return Role
     */
    public function execute(array $data): Role
    {
        return $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));
    }
}
