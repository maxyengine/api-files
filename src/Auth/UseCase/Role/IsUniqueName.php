<?php

namespace Nrg\Auth\UseCase\Role;

use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Condition\NotEqual;
use Nrg\Data\Dto\Filter;

/**
 * Class IsUniqueName
 */
class IsUniqueName
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;


    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function execute(array $data): bool
    {
        $filter = (new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['name'])
                    ->setField('name')
            );

        if (isset($data['exceptId'])) {
            $filter
                ->addCondition(
                    (new NotEqual())
                        ->setValue($data['exceptId'])
                        ->setField('id')
                );
        }

        return !$this->roleRepository->exists($filter);
    }
}