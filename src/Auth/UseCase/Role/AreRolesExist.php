<?php

namespace Nrg\Auth\UseCase\Role;

use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Data\Condition\In;
use Nrg\Data\Dto\Filter;

/**
 * Class AreRolesExist
 */
class AreRolesExist
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;


    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function execute(array $data): bool
    {
        $filter = (new Filter())
            ->addCondition(
                (new In())
                    ->setRange($data['roleIds'])
                    ->setField('id')
            );

        return count($data['roleIds']) === $this->roleRepository->countAll($filter);
    }
}