<?php

namespace Nrg\Auth\UseCase\Role;

use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Data\Collection;
use Nrg\Data\Dto\Metadata;

/**
 * Class ListRole.
 */
class ListRole
{
    /**
     * @var RoleRepository
     */
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return Collection
     */
    public function execute(array $data): Collection
    {
        $metadata = new Metadata($data);

        return $this->repository->findAll(
            $metadata->getFilter(),
            $metadata->getSorting(),
            $metadata->getPagination()
        );
    }
}
