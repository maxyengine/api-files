<?php

namespace Nrg\Auth\UseCase\Role;

use Nrg\Auth\Entity\Role;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

class DeleteRole
{
    /**
     * @var RoleRepository
     */
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(array $data): Role
    {
        $role = $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));

        $this->repository->delete($role);

        return $role;
    }
}
