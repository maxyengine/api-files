<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Data\Collection;
use Nrg\Data\Dto\Metadata;

/**
 * Class ListUser.
 */
class ListUser
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return Collection
     */
    public function execute(array $data): Collection
    {
        $metadata = new Metadata($data);

        return $this->repository->findAll(
            $metadata->getFilter(),
            $metadata->getSorting(),
            $metadata->getPagination()
        );
    }
}
