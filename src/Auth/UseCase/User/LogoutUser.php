<?php

namespace Nrg\Auth\UseCase\User;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Nrg\Auth\Entity\Token;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;

/**
 * Class LogoutUser
 */
class LogoutUser
{
    /**
     * @var TokenRepository
     */
    private $repository;

    /**
     * @param TokenRepository $repository
     */
    public function __construct(TokenRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Token $token
     *
     * @return Token
     * @throws InvalidArgumentException
     */
    public function execute(Token $token): Token
    {
        $this->repository->delete($token);

        return $token;
    }
}
