<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

class DetailsUser
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * DetailsUser constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function execute(array $data): User
    {
        return $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));
    }
}
