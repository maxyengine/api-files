<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Value\UserStatus;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

/**
 * Class ActivateUser.
 */
class ActivateUser
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function execute(array $data): User
    {
        $user = $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));

        $data['token'] = null;
        $data['status'] = UserStatus::createActive();
        $data['passwordHash'] = password_hash($data['password'], PASSWORD_DEFAULT);

        $user->populateObject($data);

        $this->repository->update($user);

        return $user;
    }
}
