<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Value\UserStatus;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Exception\EntityNotFoundException;

/**
 * Class AreValidEmailAndPassword
 */
class AreValidEmailAndPassword
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(array $data): bool
    {
        $filter = (new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['email'])
                    ->setField('email')
            )
            ->addCondition(
                (new Equal())
                    ->setValue(UserStatus::ACTIVE)
                    ->setField('status')
            )
        ;

        try {
            /**
             * @var User $user
             */
            $user = $this->userRepository->findOne($filter);
        } catch (EntityNotFoundException $e) {
            return false;
        }

        return password_verify($data['password'], $user->getPasswordHash());
    }
}