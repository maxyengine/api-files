<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Condition\In;
use Nrg\Data\Dto\Filter;

/**
 * Class AssignUserRoles.
 */
class AssignUserRoles
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * SaveUser constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository, RoleRepository $roleRepository)
    {
        $this->repository = $repository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function execute(array $data): User
    {
        $user = $this->repository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            ));

        $roles = $this->roleRepository->findAll(
            (new Filter())
                ->addCondition(
                (new In())
                    ->setRange($data['roleIdList'])
                    ->setField('id')
            )
        );

        $this->repository->saveRoles($user);

        return $user;
    }
}
