<?php

namespace Nrg\Auth\UseCase\User;

use DateTime;
use Nrg\Auth\Abstraction\AccessTokenGenerator;
use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Value\Permissions;
use Nrg\Auth\Value\UserStatus;
use Nrg\Data\Condition\In;
use Nrg\Data\Dto\Filter;
use Nrg\Utility\Abstraction\Uuid;
use Exception;

/**
 * Class CreateUser.
 */
class CreateUser
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var AccessTokenGenerator
     */
    private $tokenGenerator;

    /**
     * @var Uuid
     */
    private $uuid;

    /**
     * CreateUser constructor.
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     * @param AccessTokenGenerator $tokenGenerator
     * @param Uuid $uuid
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        AccessTokenGenerator $tokenGenerator,
        Uuid $uuid
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->tokenGenerator = $tokenGenerator;
        $this->uuid = $uuid;
    }

    /**
     * @param array $data
     *
     * @return User
     * @throws Exception
     */
    public function execute(array $data): User
    {
        $user = new User($this->uuid->generateV4());
        $roles = $this->roleRepository->findAll(
            (new Filter())
                ->addCondition(
                    (new In())
                        ->setRange($data['roles'])
                        ->setField('id')
                )
        );

        $data['createdAt'] = new DateTime();
        $data['status'] = UserStatus::createPending();
        $data['token'] = bin2hex(random_bytes(64));
        $data['roles'] = $roles;

        if (isset($data['permissions'])) {
            $data['permissions'] = new Permissions(...$data['permissions']);
        }

        $user->populateObject($data);

        $this->userRepository->create($user);

        return $user;
    }
}