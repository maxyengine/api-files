<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Value\UserStatus;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;

/**
 * Class IsUserTokenExist
 */
class IsUserTokenExist
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(array $data): bool
    {
        $filter = (new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['token'])
                    ->setField('token')
            )
            ->addCondition(
                (new Equal())
                    ->setValue($data['id'])
                    ->setField('id')
            )
            ->addCondition(
                (new Equal())
                    ->setValue(UserStatus::PENDING)
                    ->setField('status')
            )
        ;

        return $this->userRepository->exists($filter);
    }
}