<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Abstraction\AccessTokenGenerator;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Entity\Token;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;
use Exception;

/**
 * Class LoginUser
 */
class LoginUser
{
    /**
     * @var TokenRepository
     */
    private $tokenRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var AccessTokenGenerator
     */
    private $tokenGenerator;

    /**
     * @param TokenRepository $tokenRepository
     * @param UserRepository  $userRepository
     */
    public function __construct(
        TokenRepository $tokenRepository,
        UserRepository $userRepository,
        AccessTokenGenerator $tokenGenerator
    ) {
        $this->tokenRepository = $tokenRepository;
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param array $data
     *
     * @return Token
     *
     * @throws Exception
     */
    public function execute(array $data): Token
    {
        $user = $this->userRepository->findOne((new Filter())
            ->addCondition((new Equal())
                ->setValue($data['email'])
                ->setField('email')
            )
        );

        $token = $this->tokenGenerator->generate($user);

        $this->tokenRepository->create($token);

        return $token;
    }
}
