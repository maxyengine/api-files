<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Condition\NotEqual;
use Nrg\Data\Dto\Filter;

/**
 * Class IsUniqueEmail
 */
class IsUniqueEmail
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(array $data): bool
    {
        $filter = (new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($data['email'])
                    ->setField('email')
            );

        if (isset($data['exceptId'])) {
            $filter
                ->addCondition(
                    (new NotEqual())
                        ->setValue($data['exceptId'])
                        ->setField('id')
                );
        }

        return !$this->userRepository->exists($filter);
    }
}