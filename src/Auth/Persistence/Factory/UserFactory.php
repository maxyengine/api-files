<?php

namespace Nrg\Auth\Persistence\Factory;

use DateTime;
use Nrg\Auth\Abstraction\AvatarStorage;
use Nrg\Auth\Entity\User;
use Nrg\Auth\Value\Permissions;
use Nrg\Auth\Value\UserStatus;
use Nrg\Data\Abstraction\SchemaAdapter;
use Nrg\Data\Entity;
use Nrg\Data\Abstraction\AbstractFactory;
use Nrg\Http\Abstraction\RouteProvider;

/**
 * Class UserFactory.
 */
class UserFactory extends AbstractFactory
{
    /**
     * @var AvatarStorage
     */
    private $avatarStorage;

    /**
     * @var RouteProvider
     */
    private $routeProvider;

    /**
     * @param SchemaAdapter $schemaAdapter
     * @param AvatarStorage $avatarStorage
     * @param RouteProvider $routeProvider
     */
    public function __construct(
        SchemaAdapter $schemaAdapter,
        AvatarStorage $avatarStorage,
        RouteProvider $routeProvider
    ) {
        parent::__construct($schemaAdapter);

        $this->avatarStorage = $avatarStorage;
        $this->routeProvider = $routeProvider;
    }

    /**
     * @param Entity|User $entity
     *
     * @return array
     */
    public function toArray(Entity $entity): array
    {
        $array = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'email' => $entity->getEmail(),
            'status' => $entity->getStatus()->getValue(),
            'passwordHash' => $entity->getPasswordHash(),
            'description' => $entity->getDescription(),
            'permissions' => null !== $entity->getPermissions() ?
                json_encode($entity->getPermissions(), JSON_UNESCAPED_SLASHES) : null,
            'token' => $entity->getToken(),
            'createdAt' => $entity->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => null !== $entity->getUpdatedAt() ? $entity->getUpdatedAt()->format('Y-m-d H:i:s') : null,
        ];

        return $this->adaptForDB($array);
    }

    /**
     * {@inheritdoc}
     */
    public function createEntity(array $data): Entity
    {
        $data = $this->adaptForApp($data);

        $data['status'] = new UserStatus($data['status']);
        $data['permissions'] = null !== $data['permissions'] ?
            $this->createPermissions(json_decode($data['permissions'])) : null;
        $data['avatar'] = $this->avatarStorage->getUrl($data['id']);
        $data['createdAt'] = new DateTime($data['createdAt']);
        $data['updatedAt'] = null !== $data['updatedAt'] ? new DateTime($data['updatedAt']) : null;

        $entity = new User($data['id']);
        $entity->populateObject($data);

        return $entity;
    }

    private function createPermissions(array $data): Permissions
    {
        $permissions = new Permissions();

        foreach ($data as $permission) {
            $parsed = parse_url($permission);
            parse_str($parsed['query'] ?? '', $params);
            $path = $parsed['path'];

            $permissions->addUrl($this->routeProvider->createUrl($path, $params));
        }

        return $permissions;
    }
}
