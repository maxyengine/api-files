<?php

namespace Nrg\Auth\Persistence\Factory;

use DateTime;
use Nrg\Auth\Entity\Token;
use Nrg\Data\Abstraction\AbstractFactory;
use Nrg\Data\Entity;

/**
 * Class TokenFactory.
 */
class TokenFactory extends AbstractFactory
{
    /**
     * @param Entity|Token $entity
     *
     * @return array
     */
    public function toArray(Entity $entity): array
    {
        $array = [
            'id' => $entity->getId(),
            'userId' => $entity->getUser()->getId(),
            'createdAt' => $entity->getCreatedAt()->format('Y-m-d H:i:s'),
            'expires' => $entity->getExpires()->format('Y-m-d H:i:s'),
        ];

        return $this->adaptForDB($array);
    }

    /**
     * {@inheritdoc}
     */
    public function createEntity(array $data): Entity
    {
        $data = $this->adaptForApp($data);

        $data['createdAt'] = new DateTime($data['createdAt']);
        $data['expires'] = new DateTime($data['expires']);

        $entity = new Token($data['id']);
        $entity->populateObject($data);

        return $entity;
    }
}
