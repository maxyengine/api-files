<?php

namespace Nrg\Auth\Persistence\Factory;

use DateTime;
use Nrg\Auth\Entity\Role;
use Nrg\Auth\Value\Permissions;
use Nrg\Data\Abstraction\SchemaAdapter;
use Nrg\Data\Entity;
use Nrg\Data\Abstraction\AbstractFactory;
use Nrg\Http\Abstraction\RouteProvider;

/**
 * Class RoleFactory.
 */
class RoleFactory extends AbstractFactory
{
    /**
     * @var RouteProvider
     */
    private $routeProvider;

    /**
     * @param SchemaAdapter $schemaAdapter
     * @param RouteProvider $routeProvider
     */
    public function __construct(SchemaAdapter $schemaAdapter, RouteProvider $routeProvider)
    {
        parent::__construct($schemaAdapter);

        $this->routeProvider = $routeProvider;
    }

    /**
     * @param Entity|Role $entity
     *
     * @return array
     */
    public function toArray(Entity $entity): array
    {
        $array = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'description' => $entity->getDescription(),
            'permissions' => json_encode($entity->getPermissions(), JSON_UNESCAPED_SLASHES),
            'createdAt' => $entity->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => (null === $entity->getUpdatedAt()) ? null : $entity->getUpdatedAt()->format('Y-m-d H:i:s'),
        ];

        return $this->adaptForDB($array);
    }

    /**
     * @param array $data
     *
     * @return Role
     */
    public function createEntity(array $data): Entity
    {
        $data = $this->adaptForApp($data);

        $data['permissions'] = $this->createPermissions(json_decode($data['permissions']));
        $data['createdAt'] = new DateTime($data['createdAt']);
        $data['updatedAt'] = null !== $data['updatedAt'] ? new DateTime($data['updatedAt']) : null;

        $entity = new Role($data['id']);
        $entity->populateObject($data);

        return $entity;
    }

    private function createPermissions(array $data): Permissions
    {
        $permissions = new Permissions();

        foreach ($data as $permission) {
            $parsed = parse_url($permission);
            parse_str($parsed['query'] ?? '', $params);
            $path = $parsed['path'];

            $permissions->addUrl($this->routeProvider->createUrl($path, $params));
        }

        return $permissions;
    }
}
