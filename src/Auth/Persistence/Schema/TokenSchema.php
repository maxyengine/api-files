<?php

namespace Nrg\Auth\Persistence\Schema;

use Nrg\Auth\Persistence\Factory\TokenFactory;
use Nrg\Data\Abstraction\AbstractFactory;
use Nrg\Data\Abstraction\AbstractSchema;

/**
 * Class TokenSchema
 */
class TokenSchema extends AbstractSchema
{
    private const TABLE_NAME = 'nrgsoft_auth_token';

    /**
     * {@inheritdoc}
     */
    public function getFieldNames(): array
    {
        return [
            'id',
            'user_id',
            'created_at',
            'expires',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function createFactory(): ?AbstractFactory
    {
        return new TokenFactory($this->getAdapter());
    }
}
