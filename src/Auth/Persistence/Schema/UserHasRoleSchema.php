<?php

namespace Nrg\Auth\Persistence\Schema;

use Nrg\Data\Abstraction\AbstractFactory;
use Nrg\Data\Abstraction\AbstractSchema;

class UserHasRoleSchema extends AbstractSchema
{
    private const TABLE_NAME = 'nrgsoft_auth_user_has_role';

    /**
     * {@inheritdoc}
     */
    public function getFieldNames(): array
    {
        return [
            'user_id',
            'role_id',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function createFactory(): ?AbstractFactory
    {
        return null;
    }
}
