<?php

namespace Nrg\Auth\Persistence\Schema;

use Nrg\Data\Abstraction\AbstractSchema;

class UserSchema extends AbstractSchema
{
    private const TABLE_NAME = 'nrgsoft_auth_user';

    /**
     * {@inheritdoc}
     */
    public function getFieldNames(): array
    {
        return [
            'id',
            'name',
            'email',
            'status',
            'password_hash',
            'description',
            'permissions',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
