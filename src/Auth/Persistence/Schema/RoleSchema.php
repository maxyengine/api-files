<?php

namespace Nrg\Auth\Persistence\Schema;

use Nrg\Auth\Persistence\Factory\RoleFactory;
use Nrg\Data\Abstraction\AbstractFactory;
use Nrg\Data\Abstraction\AbstractSchema;

class RoleSchema extends AbstractSchema
{
    private const TABLE_NAME = 'nrgsoft_auth_role';

    /**
     * {@inheritdoc}
     */
    public function getFieldNames(): array
    {
        return [
            'id',
            'name',
            'description',
            'permissions',
            'created_at',
            'updated_at',
            'number_of_users'
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function createFactory(): AbstractFactory
    {
        return new RoleFactory($this->getAdapter());
    }
}
