<?php

namespace Nrg\Auth\Persistence\Abstraction;

use Doctrine\DBAL\DBALException;
use Nrg\Auth\Entity\User;
use Nrg\Data\Collection;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Dto\Pagination;
use Nrg\Data\Dto\Sorting;
use Nrg\Data\Entity;
use Nrg\Data\Exception\EntityNotFoundException;
use RuntimeException;

/**
 * Interface UserRepository.
 */
interface UserRepository
{
    /**
     * @param User $user
     *
     * @throws DBALException
     */
    public function create(User $user): void;

    /**
     * @param User $user
     */
    public function update(User $user): void;

    /**
     * @param User $user
     */
    public function addAuthToken(User $user): void;

    /**
     * @param Filter $filter
     *
     * @throws EntityNotFoundException
     *
     * @return Entity|User
     */
    public function findOne(Filter $filter): User;

    /**
     * @param Filter $filter
     *
     * @throws RuntimeException
     *
     * @return bool
     */
    public function exists(Filter $filter): bool;

    /**
     * @param Filter|null     $filter
     * @param Sorting|null    $sorting
     * @param Pagination|null $pagination
     *
     * @return Collection
     */
    public function findAll(Filter $filter = null, Sorting $sorting = null, Pagination $pagination = null): Collection;
}
