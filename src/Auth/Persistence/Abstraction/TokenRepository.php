<?php

namespace Nrg\Auth\Persistence\Abstraction;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Nrg\Auth\Entity\Token;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Entity;
use Nrg\Data\Exception\EntityNotFoundException;

/**
 * Interface TokenRepository
 */
interface TokenRepository
{
    /**
     * @param Token $token
     */
    public function create(Token $token): void;

    /**
     * @param Filter $filter
     *
     * @throws EntityNotFoundException
     *
     * @return Entity|Token
     */
    public function findOne(Filter $filter): Token;

    /**
     * @param Filter $filter
     *
     * @return bool
     */
    public function exists(Filter $filter): bool;

    /**
     * @param Token $token
     *
     * @throws InvalidArgumentException
     */
    public function delete(Token $token): void;
}
