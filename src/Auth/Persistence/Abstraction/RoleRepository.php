<?php

namespace Nrg\Auth\Persistence\Abstraction;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Nrg\Auth\Entity\Role;
use Nrg\Data\Collection;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Dto\Pagination;
use Nrg\Data\Dto\Sorting;
use Nrg\Data\Entity;
use Nrg\Data\Exception\EntityNotFoundException;

/**
 * Interface RoleRepository.
 */
interface RoleRepository
{
    /**
     * @param Role $role
     */
    public function create(Role $role): void;

    /**
     * @param Role $role
     */
    public function update(Role $role): void;

    /**
     * @param Role $role
     *
     * @throws InvalidArgumentException
     */
    public function delete(Role $role): void;

    /**
     * @param Filter $filter
     *
     * @return bool
     */
    public function exists(Filter $filter): bool;

    /**
     * @param null|Filter     $filter
     * @param null|Sorting    $sorting
     * @param null|Pagination $pagination
     *
     * @return Collection
     */
    public function findAll(Filter $filter = null, Sorting $sorting = null, Pagination $pagination = null): Collection;

    /**
     * @param null|Filter $filter
     * @param bool        $withNumberOfUsers
     *
     * @return int
     */
    public function countAll(?Filter $filter = null, $withNumberOfUsers = false): int;

    /**
     * @param Filter $filter
     *
     * @throws EntityNotFoundException
     *
     * @return Entity|Role
     */
    public function findOne(Filter $filter): Role;

    /**
     * @param array $userIds
     *
     * @return array
     */
    public function findAllByUserIds(array $userIds): array;
}
