<?php

namespace Nrg\Auth\Persistence\Repository;

use Doctrine\DBAL\Connection;
use Nrg\Auth\Entity\Role;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Auth\Persistence\Factory\RoleFactory;
use Nrg\Auth\Persistence\Schema\RoleSchema;
use Nrg\Auth\Persistence\Schema\UserHasRoleSchema;
use Nrg\Data\Collection;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Dto\Pagination;
use Nrg\Data\Dto\Sorting;
use Nrg\Data\Entity;
use Nrg\Doctrine\EntityRepository;
use Nrg\Data\Exception\EntityNotFoundException;
use Nrg\Doctrine\Scope\FilterScope;
use Nrg\Doctrine\Scope\PaginationScope;
use Nrg\Doctrine\Scope\SortingScope;

/**
 * Class DbalRoleRepository.
 */
class DbalRoleRepository extends EntityRepository implements RoleRepository
{
    /**
     * @var UserHasRoleSchema
     */
    private $hasUserSchema;

    /**
     * @param RoleFactory       $factory
     * @param RoleSchema        $schema
     * @param UserHasRoleSchema $hasUserSchema
     */
    public function __construct(
        RoleFactory $factory,
        RoleSchema $schema,
        UserHasRoleSchema $hasUserSchema
    ) {
        parent::__construct($schema, $factory);

        $this->hasUserSchema = $hasUserSchema;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Role $role): void
    {
        $this->getConnection()->insert(
            $this->getFullTableName(),
            $this->getFactory()->toArray($role)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function update(Role $role): void
    {
        $this->getConnection()->update(
            $this->getFullTableName(),
            $this->getFactory()->toArray($role),
            ['id' => $role->getId()]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Role $role): void
    {
        $this->getConnection()->delete(
            $this->getFullTableName(),
            ['id' => $role->getId()]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function exists(Filter $filter): bool
    {
        return false !== $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter)
        )
        ->select('id')
        ->from($this->getFullTableName())
        ->setMaxResults(1)
        ->execute()
        ->fetch();
    }

    /**
     * @param Filter $filter
     *
     * @throws EntityNotFoundException
     *
     * @return Entity|Role
     */
    public function findOne(Filter $filter): Role
    {
        $numberOfUsersSQL = $this->getNumberOfUsersSQL();

        $data = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter)
        )
            ->select($this->getFieldNames())
            ->from($this->getFullTableName(), 'r')
            ->leftJoin('r', "({$numberOfUsersSQL})", 'un', 'r.id = un.rid')
            ->execute()
            ->fetch();

        if (false === $data) {
            throw new EntityNotFoundException('Entity was not found');
        }

        return $this->getFactory()->createEntity($data);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(Filter $filter = null, Sorting $sorting = null, Pagination $pagination = null): Collection
    {
        $numberOfUsersSQL = $this->getNumberOfUsersSQL();

        $data = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter),
            new SortingScope($this->getSchemaAdapter(), $sorting),
            new PaginationScope($pagination)
        )
        ->select($this->getFieldNames())
        ->from($this->getFullTableName(), 'r')
        ->leftJoin('r', "({$numberOfUsersSQL})", 'un', 'r.id = un.rid')
        ->execute()
        ->fetchAll();

        $total = $this->countAll($filter, true);

        return $this->getFactory()
            ->createCollection($data)
            ->setTotal($total)
            ->setPagination($pagination);
    }

    /**
     * {@inheritdoc}
     */
    public function countAll(?Filter $filter = null, $withNumberOfUsers = false): int
    {
        $query = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter)
        )
        ->select('COUNT(*)')
        ->from($this->getFullTableName(), 'r');

        if ($withNumberOfUsers) {
            $numberOfUsersSQL = $this->getNumberOfUsersSQL();
            $query->leftJoin('r', "({$numberOfUsersSQL})", 'un', 'r.id = un.rid');
        }

        return $query->execute()->fetchColumn();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByUserIds(array $userIds): array
    {
        $fieldNames = $this->getFieldNames();
        unset($fieldNames[array_search('number_of_users', $fieldNames)]);

        $data = $this->createQuery()
            ->select('user_id', ...$fieldNames)
            ->from($this->hasUserSchema->getFullTableName(), 'uhr')
            ->leftJoin('uhr', $this->getFullTableName(), 'r', 'uhr.role_id = r.id')
            ->where('user_id IN(?)')
            ->setParameter(0, $userIds, Connection::PARAM_STR_ARRAY)
            ->execute()
            ->fetchAll();

        $userHasRoles = [];
        foreach ($data as $item) {
            $userHasRoles[$item['user_id']][] = $item;
        }

        foreach ($userHasRoles as $userId => $items) {
            $userHasRoles[$userId] = $this->getFactory()->createCollection($items);
        }

        return $userHasRoles;
    }

    /**
     * @return string
     */
    private function getNumberOfUsersSQL(): string
    {
        return $this->createQuery()
            ->select('id rid', 'COUNT(user_id) number_of_users')
            ->from($this->getFullTableName(), 'jr')
            ->leftJoin('jr', $this->hasUserSchema->getFullTableName(), 'uhr', 'uhr.role_id = jr.id')
            ->groupBy('rid')
            ->getSQL();
    }
}
