<?php

namespace Nrg\Auth\Persistence\Repository;

use Nrg\Auth\Entity\Token;
use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\TokenRepository;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Persistence\Factory\TokenFactory;
use Nrg\Auth\Persistence\Schema\TokenSchema;
use Nrg\Data\Condition\Equal;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Entity;
use Nrg\Data\Exception\EntityNotFoundException;
use Nrg\Doctrine\EntityRepository;
use Nrg\Doctrine\Scope\FilterScope;

/**
 * Class DbalTokenRepository
 */
class DbalTokenRepository extends EntityRepository implements TokenRepository
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        TokenFactory $factory,
        TokenSchema $schema,
        UserRepository $userRepository
    ) {
        parent::__construct($schema, $factory);

        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Token $token): void
    {
        $this->getConnection()->insert(
            $this->getFullTableName(),
            $this->getFactory()->toArray($token)
        );
    }

    /**
     * @param Filter $filter
     *
     * @throws EntityNotFoundException
     *
     * @return Entity|Token
     */
    public function findOne(Filter $filter): Token
    {
        $data = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter)
        )
            ->select($this->getFieldNames())
            ->from($this->getFullTableName())
            ->execute()
            ->fetch();

        if (false === $data) {
            throw new EntityNotFoundException('Entity was not found');
        }

        $data['user'] = $this->findUser($data['user_id']);

        return $this->getFactory()->createEntity($data);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(Filter $filter): bool
    {
        return false !==  $this->createQuery(
                new FilterScope($this->getSchemaAdapter(), $filter)
            )
                ->select('id')
                ->from($this->getFullTableName())
                ->setMaxResults(1)
                ->execute()
                ->fetch();
    }


    /**
     * {@inheritdoc}
     */
    public function delete(Token $token): void
    {
        $this->getConnection()->delete(
            $this->getFullTableName(),
            ['id' => $token->getId()]
        );
    }

    /**
     * @param string $id
     *
     * @return User
     */
    private function findUser(string $id): User
    {
        return $this->userRepository->findOne((new Filter())
            ->addCondition(
                (new Equal())
                    ->setValue($id)
                    ->setField('id')
            ));
    }
}
