<?php

namespace Nrg\Auth\Persistence\Repository;

use Nrg\Auth\Entity\User;
use Nrg\Auth\Persistence\Abstraction\RoleRepository;
use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Persistence\Factory\UserFactory;
use Nrg\Auth\Persistence\Schema\TokenSchema;
use Nrg\Auth\Persistence\Schema\UserHasRoleSchema;
use Nrg\Auth\Persistence\Schema\UserSchema;
use Nrg\Data\Collection;
use Nrg\Data\Condition\In;
use Nrg\Data\Dto\Filter;
use Nrg\Data\Dto\Pagination;
use Nrg\Data\Dto\Sorting;
use Nrg\Data\Entity;
use Nrg\Data\Exception\EntityNotFoundException;
use Nrg\Doctrine\Command\MultipleInsert;
use Nrg\Doctrine\EntityRepository;
use Nrg\Doctrine\Scope\FilterScope;
use Nrg\Doctrine\Scope\PaginationScope;
use Nrg\Doctrine\Scope\SortingScope;
use PDO;

/**
 * Class DbalUserRepository.
 */
class DbalUserRepository extends EntityRepository implements UserRepository
{
    /**
     * @var UserHasRoleSchema
     */
    private $hasRoleSchema;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var TokenSchema
     */
    private $tokenSchema;

    /**
     * @param UserFactory $factory
     * @param UserSchema $schema
     * @param UserHasRoleSchema $hasRoleSchema
     * @param TokenSchema $tokenSchema
     * @param RoleRepository $roleRepository
     */
    public function __construct(
        UserFactory $factory,
        UserSchema $schema,
        UserHasRoleSchema $hasRoleSchema,
        TokenSchema $tokenSchema,
        RoleRepository $roleRepository
    ) {
        parent::__construct($schema, $factory);

        $this->hasRoleSchema = $hasRoleSchema;
        $this->tokenSchema = $tokenSchema;
        $this->roleRepository = $roleRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function create(User $user): void
    {
        $this->getConnection()->beginTransaction();

        $this->getConnection()->insert(
            $this->getFullTableName(),
            $this->getFactory()->toArray($user)
        );

        $command = (new MultipleInsert($this->getConnection()))
            ->setTableName($this->hasRoleSchema->getFullTableName())
            ->setColumns($this->hasRoleSchema->getFieldNames());

        foreach ($user->getRoles() as $role) {
            $command->addValues([$user->getId(), $role->getId()]);
        }

        $command->execute();

        $this->getConnection()->commit();
    }

    /**
     * {@inheritdoc}
     */
    public function update(User $user): void
    {
        $this->getConnection()->update(
            $this->getFullTableName(),
            $this->getFactory()->toArray($user),
            ['id' => $user->getId()]
        );
    }

    public function addAuthToken(User $user): void
    {
        $this->getConnection()->insert(
            $this->tokenSchema->getFullTableName(),
            $this->getFactory()->toArray($user)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function exists(Filter $filter): bool
    {
        return false !== $this->createQuery(
                new FilterScope($this->getSchemaAdapter(), $filter)
            )
                ->select('id')
                ->from($this->getFullTableName())
                ->setMaxResults(1)
                ->execute()
                ->fetch();
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(Filter $filter = null, Sorting $sorting = null, Pagination $pagination = null): Collection
    {
        $data = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter),
            new SortingScope($this->getSchemaAdapter(), $sorting),
            new PaginationScope($pagination)
        )
            ->select($this->getFieldNames())
            ->from($this->getFullTableName())
            ->execute()
            ->fetchAll(PDO::FETCH_UNIQUE);

        $roles = $this->roleRepository->findAllByUserIds(array_keys($data));

        foreach ($data as $id => $item) {
            $data[$id] += [
                'id' => $id,
                'roles' => $roles[$id] ?? new Collection(),
            ];
        }

        $total = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter)
        )
            ->select('COUNT(id)')
            ->from($this->getFullTableName())
            ->execute()
            ->fetchColumn();

        return $this->getFactory()
            ->createCollection($data)
            ->setTotal($total)
            ->setPagination($pagination);
    }

    /**
     * @param Filter $filter
     *
     * @throws EntityNotFoundException
     *
     * @return Entity|User
     */
    public function findOne(Filter $filter): User
    {
        $data = $this->createQuery(
            new FilterScope($this->getSchemaAdapter(), $filter)
        )
            ->select($this->getFieldNames())
            ->from($this->getFullTableName())
            ->execute()
            ->fetch();

        if (false === $data) {
            throw new EntityNotFoundException('Entity was not found');
        }

        $data['roles'] = $this->findRoles($data['id']);

        return $this->getFactory()->createEntity($data);
    }

    private function findRoles(string $userId): Collection
    {
        $roleIds = $this->createQuery()
            ->select('role_id')
            ->from($this->hasRoleSchema->getFullTableName())
            ->where('user_id = ?')
            ->setParameter(0, $userId)
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN);

        return $this->roleRepository->findAll(
            (new Filter())
                ->addCondition(
                    (new In())
                        ->setRange($roleIds)
                        ->setField('id')
                )
        );
    }
}
