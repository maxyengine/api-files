<?php

namespace Nrg\Auth\Entity;

use DateTime;
use Nrg\Auth\Value\Permissions;
use Nrg\Auth\Value\UserStatus;
use Nrg\Data\Collection;
use Nrg\Data\Entity;
use Nrg\Data\Service\PopulateAbility;
use Nrg\Http\Value\Url;
use JsonSerializable;

/**
 * Class User.
 */
class User extends Entity implements JsonSerializable
{
    use PopulateAbility;

    /**
     * @var string;
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var UserStatus
     */
    private $status;

    /**
     * @var null|string
     */
    private $passwordHash;

    /**
     * @var null|string
     */
    private $description;

    /**
     * @var null|string
     */
    private $token;

    /**
     * @var null|Url
     */
    private $avatar;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var null|DateTime
     */
    private $updatedAt;

    /**
     * @var Permissions|null
     */
    private $permissions;

    /**
     * @var Collection|Role[]
     */
    private $roles;

    /**
     * @param string $id
     */
    public function __construct(string $id)
    {
        parent::__construct($id);

        $this->permissions = new Permissions();
        $this->roles = new Collection();
    }

    /**
     * @return bool
     */
    public function isSuperUser(): bool
    {
        return null === $this->permissions;
    }

    /**
     * @param Url $verify
     *
     * @return bool
     */
    public function isAllowed(Url $verify): bool
    {
        if ($this->isSuperUser()) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role->isAllowed($verify)) {
                return true;
            }
        }

        return $this->getPermissions()->isAllowed($verify);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return Permissions|null
     */
    public function getPermissions(): ?Permissions
    {
        return $this->permissions;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        $data = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'status' => $this->getStatus()->getValue(),
            'avatar' => null !== $this->getAvatar() ? (string) $this->getAvatar() : null,
            'description' => $this->getDescription(),
            'createdAt' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => null !== $this->getUpdatedAt() ? $this->getUpdatedAt()->format('Y-m-d H:i:s') : null,
            'roles' => $this->getRoles(),
            'permissions' => $this->getPermissions(),
        ];

        return $data;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    /**
     * @return Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * @return null|string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return null|Url
     */
    public function getAvatar(): ?Url
    {
        return $this->avatar;
    }

    /**
     * @return UserStatus
     */
    public function getStatus(): UserStatus
    {
        return $this->status;
    }

    /**
     * @param UserStatus $status
     */
    private function setStatus(UserStatus $status)
    {
        $this->status = $status;
    }

    /**
     * @param null|Url $avatar
     */
    private function setAvatar(?Url $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @param null|string $token
     */
    private function setToken(?string $token)
    {
        $this->token = $token;
    }

    /**
     * @param Collection $roles
     */
    private function setRoles(Collection $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @param string $passwordHash
     */
    private function setPasswordHash(?string $passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @param string $email
     */
    private function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param string $name
     */
    private function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    private function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param DateTime $createdAt
     */
    private function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    private function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Permissions|null $permissions
     */
    private function setPermissions(?Permissions $permissions): void
    {
        $this->permissions = $permissions;
    }
}
