<?php

namespace Nrg\Auth\Entity;

use DateTime;
use Nrg\Data\Entity;
use Nrg\Data\Service\PopulateAbility;
use JsonSerializable;

/**
 * Class Token.
 */
class Token extends Entity implements JsonSerializable
{
    use PopulateAbility;

    /**
     * @var User
     */
    private $user;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var DateTime
     */
    private $expires;

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'user' => $this->getUser(),
            'createdAt' => $this->getCreatedAt()->format('Y-m-d H:i:s') . ' UTC',
            'expires' => $this->getExpires()->getTimestamp() - time(),
            'ttl' => $this->getExpires()->getTimestamp() - $this->getCreatedAt()->getTimestamp()
        ];
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getExpires(): DateTime
    {
        return $this->expires;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    private function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param DateTime $expires
     */
    private function setExpires(DateTime $expires)
    {
        $this->expires = $expires;
    }

    /**
     * @param DateTime $createdAt
     */
    private function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
