<?php

namespace Nrg\Auth\Entity;

use DateTime;
use Nrg\Auth\Value\Permissions;
use Nrg\Data\Entity;
use Nrg\Data\Service\PopulateAbility;
use JsonSerializable;
use Nrg\Http\Value\Url;

/**
 * Class Role.
 */
class Role extends Entity implements JsonSerializable
{
    use PopulateAbility;

    /**
     * @var string;
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @var Permissions
     */
    private $permissions;

    /**
     * @var int
     */
    private $numberOfUsers;

    /**
     * @param string $id
     */
    public function __construct(string $id)
    {
        parent::__construct($id);

        $this->permissions = new Permissions();
    }

    /**
     * @param Url $verify
     *
     * @return bool
     */
    public function isAllowed(Url $verify): bool
    {
        return $this->getPermissions()->isAllowed($verify);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return Permissions
     */
    public function getPermissions(): Permissions
    {
        return $this->permissions;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        $data = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'createdAt' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => (null === $this->getUpdatedAt()) ? null : $this->getUpdatedAt()->format('Y-m-d H:i:s'),
            'numberOfUsers' => $this->numberOfUsers,
            'permissions' => $this->getPermissions(),
        ];

        return $data;
    }

    /**
     * @param int $numberOfUsers
     */
    private function setNumberOfUsers(?int $numberOfUsers)
    {
        $this->numberOfUsers = $numberOfUsers;
    }

    /**
     * @param string $name
     */
    private function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    private function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param DateTime $createdAt
     */
    private function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    private function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param Permissions $permissions
     */
    private function setPermissions(Permissions $permissions): void
    {
        $this->permissions = $permissions;
    }
}
