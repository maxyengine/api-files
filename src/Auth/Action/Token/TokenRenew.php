<?php

namespace Nrg\Auth\Action\Token;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\UseCase\Token\RenewToken;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class TokenRenew
 */
class TokenRenew implements Observer
{
    use ObserverStub;

    /**
     * @var AccessControl
     */
    private $accessControl;

    /**
     * @var RenewToken
     */
    private $renewToken;

    /**
     * @param AccessControl $accessControl
     * @param RenewToken $renewToken
     */
    public function __construct(AccessControl $accessControl, RenewToken $renewToken)
    {
        $this->accessControl = $accessControl;
        $this->renewToken = $renewToken;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        $event->getResponse()
            ->setStatus(new HttpStatus(HttpStatus::CREATED))
            ->setBody(
                $this->renewToken->execute(
                    $this->accessControl->getToken()
                )
            );
    }
}
