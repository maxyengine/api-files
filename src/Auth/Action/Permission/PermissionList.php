<?php

namespace Nrg\Auth\Action\Permission;

use Nrg\Http\Abstraction\RouteProvider;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Nrg\Auth\Value\Permissions as Permissions;

/**
 * Class PermissionList.
 */
class PermissionList implements Observer
{
    use ObserverStub;

    /**
     * @var RouteProvider
     */
    private $routeProvider;

    /**
     * PermissionList constructor.
     *
     * @param RouteProvider $routeProvider
     */
    public function __construct(RouteProvider $routeProvider)
    {
        $this->routeProvider = $routeProvider;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $event->getResponse()
            ->setStatus(new HttpStatus(HttpStatus::OK))
            ->setBody(new Permissions(...$this->routeProvider->getRoutePaths()));
    }
}
