<?php

namespace Nrg\Auth\Action\Role;

use Nrg\Auth\UseCase\Role\DeleteRole;
use Nrg\Form\Form\RequiredUuidForm;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class RoleDelete.
 */
class RoleDelete implements Observer
{
    use ObserverStub;

    /**
     * @var RequiredUuidForm
     */
    private $form;

    /**
     * @var DeleteRole
     */
    private $deleteRole;

    /**
     * RoleDelete constructor.
     *
     * @param RequiredUuidForm $form
     * @param DeleteRole       $deleteRole
     */
    public function __construct(RequiredUuidForm $form, DeleteRole $deleteRole)
    {
        $this->form = $form;
        $this->deleteRole = $deleteRole;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::ACCEPTED))
                ->setBody($this->deleteRole->execute($this->form->serialize()));
        }
    }
}
