<?php

namespace Nrg\Auth\Action\Role;

use Nrg\Auth\UseCase\Role\ListRole;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class RoleList.
 */
class RoleList implements Observer
{
    use ObserverStub;

    /**
     * @var ListRole
     */
    private $listRole;

    /**
     * RoleList constructor.
     *
     * @param ListRole $listRole
     */
    public function __construct(ListRole $listRole)
    {
        $this->listRole = $listRole;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $event->getResponse()
            ->setStatus(new HttpStatus(HttpStatus::OK))
            ->setBody($this->listRole->execute($event->getRequest()->getBodyParams()));
    }
}
