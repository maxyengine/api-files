<?php

namespace Nrg\Auth\Action\Role;

use Nrg\Auth\UseCase\Role\DetailsRole;
use Nrg\Form\Form\RequiredUuidForm;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class RoleDetails.
 */
class RoleDetails implements Observer
{
    use ObserverStub;

    /**
     * @var RequiredUuidForm
     */
    private $form;

    /**
     * @var DetailsRole
     */
    private $detailsRole;

    /**
     * RoleDetailsView constructor.
     *
     * @param RequiredUuidForm $form
     * @param DetailsRole      $detailsRole
     */
    public function __construct(RequiredUuidForm $form, DetailsRole $detailsRole)
    {
        $this->form = $form;
        $this->detailsRole = $detailsRole;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::OK))
                ->setBody($this->detailsRole->execute($this->form->serialize()));
        }
    }
}
