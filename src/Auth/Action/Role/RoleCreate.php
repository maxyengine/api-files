<?php

namespace Nrg\Auth\Action\Role;

use Nrg\Auth\Form\Role\CreateRoleForm;
use Nrg\Auth\UseCase\Role\CreateRole;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class RoleCreate.
 */
class RoleCreate implements Observer
{
    use ObserverStub;

    /**
     * @var CreateRoleForm
     */
    private $form;

    /**
     * @var CreateRole
     */
    private $createRole;

    /**
     * RoleCreate constructor.
     *
     * @param CreateRoleForm $form
     * @param CreateRole     $createRole
     */
    public function __construct(CreateRoleForm $form, CreateRole $createRole)
    {
        $this->form = $form;
        $this->createRole = $createRole;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::CREATED))
                ->setBody($this->createRole->execute($this->form->serialize()));
        }
    }
}
