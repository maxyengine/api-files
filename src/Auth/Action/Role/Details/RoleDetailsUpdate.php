<?php

namespace Nrg\Auth\Action\Role\Details;

use Nrg\Auth\Form\Role\Details\UpdateRoleDetailsForm;
use Nrg\Auth\UseCase\Role\Details\UpdateRoleDetails;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class RoleDetailsUpdate.
 */
class RoleDetailsUpdate implements Observer
{
    use ObserverStub;

    /**
     * @var UpdateRoleDetailsForm
     */
    private $form;

    /**
     * @var UpdateRoleDetails
     */
    private $detailsRole;

    public function __construct(UpdateRoleDetailsForm $form, UpdateRoleDetails $detailsRole)
    {
        $this->form = $form;
        $this->detailsRole = $detailsRole;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::OK))
                ->setBody($this->detailsRole->execute($this->form->serialize()));
        }
    }
}
