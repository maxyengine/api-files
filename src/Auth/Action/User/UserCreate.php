<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Form\User\CreateUserForm;
use Nrg\Auth\UseCase\User\CreateUser;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class UserCreate.
 */
class UserCreate implements Observer
{
    use ObserverStub;

    /**
     * @var CreateUserForm
     */
    private $form;

    /**
     * @var CreateUser
     */
    private $createUser;

    /**
     * UserCreate constructor.
     *
     * @param CreateUserForm $form
     * @param CreateUser $createUser
     */
    public function __construct(CreateUserForm $form, CreateUser $createUser)
    {
        $this->form = $form;
        $this->createUser = $createUser;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setBody($this->form->serialize())
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY));
        } else {
            $event->getResponse()
                ->setBody($this->createUser->execute($this->form->serialize()))
                ->setStatus(new HttpStatus(HttpStatus::CREATED));
        }
    }
}
