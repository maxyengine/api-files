<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\UseCase\User\DetailsUser;
use Nrg\Form\Form\RequiredUuidForm;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class UserDetails.
 */
class UserDetails implements Observer
{
    use ObserverStub;

    /**
     * @var RequiredUuidForm
     */
    private $form;

    /**
     * @var DetailsUser
     */
    private $detailsUser;

    /**
     * UserDetailsView constructor.
     *
     * @param RequiredUuidForm $form
     * @param DetailsUser      $detailsUser
     */
    public function __construct(RequiredUuidForm $form, DetailsUser $detailsUser)
    {
        $this->form = $form;
        $this->detailsUser = $detailsUser;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());
        if ($this->form->hasErrors()) {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY));
            $event->getResponse()->setBody($this->form->serialize());
        } else {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::OK));
            $event->getResponse()->setBody($this->detailsUser->execute($this->form->serialize()));
        }
    }
}
