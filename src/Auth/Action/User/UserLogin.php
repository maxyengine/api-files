<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\Form\User\LoginUserForm;
use Nrg\Auth\UseCase\User\LoginUser;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class UserLogin
 */
class UserLogin implements Observer
{
    use ObserverStub;

    /**
     * @var LoginUserForm
     */
    private $form;

    /**
     * @var LoginUser
     */
    private $loginUser;

    /**
     * @var AccessControl
     */
    private $accessControl;

    /**
     * @param AccessControl $accessControl
     * @param LoginUserForm $form
     * @param LoginUser $loginUser
     */
    public function __construct(AccessControl $accessControl, LoginUserForm $form, LoginUser $loginUser)
    {
        $this->accessControl = $accessControl;
        $this->form = $form;
        $this->loginUser = $loginUser;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        if ($this->accessControl->hasToken()) {
            $this->loginByToken($event);
        } else {
            $this->loginByEmail($event);
        }
    }

    /**
     * @param HttpExchangeEvent $event
     */
    private function loginByToken(HttpExchangeEvent $event) {
        $event->getResponse()
            ->setStatus(new HttpStatus(HttpStatus::OK))
            ->setBody($this->accessControl->getToken());
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    private function loginByEmail(HttpExchangeEvent $event) {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::CREATED))
                ->setBody($this->loginUser->execute($this->form->serialize()));
        }
    }
}
