<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Form\User\ActivateUserForm;
use Nrg\Auth\UseCase\User\ActivateUser;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class UserActivate
 */
class UserActivate implements Observer
{
    use ObserverStub;

    /**
     * @var ActivateUserForm
     */
    private $form;

    /**
     * @var ActivateUser
     */
    private $activateUser;

    /**
     * @param ActivateUserForm $form
     * @param ActivateUser     $activateUser
     */
    public function __construct(ActivateUserForm $form, ActivateUser $activateUser)
    {
        $this->form = $form;
        $this->activateUser = $activateUser;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::CREATED))
                ->setBody($this->activateUser->execute($this->form->serialize()));
        }
    }
}
