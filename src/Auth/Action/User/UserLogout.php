<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\UseCase\User\LogoutUser;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class UserLogout
 */
class UserLogout implements Observer
{
    use ObserverStub;

    /**
     * @var AccessControl
     */
    private $accessControl;

    /**
     * @var LogoutUser
     */
    private $logoutUser;


    public function __construct(AccessControl $accessControl, LogoutUser $logoutUser)
    {
        $this->accessControl = $accessControl;
        $this->logoutUser = $logoutUser;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        $event->getResponse()
            ->setStatus(new HttpStatus(HttpStatus::OK))
            ->setBody($this->logoutUser->execute($this->accessControl->getToken()));
    }
}
