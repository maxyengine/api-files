<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\Abstraction\AvatarStorage;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class ProfileUploadAvatar
 */
class ProfileUploadAvatar implements Observer
{
    use ObserverStub;

    /**
     * @var AccessControl
     */
    private $accessControl;
    /**
     * @var AvatarStorage
     */
    private $avatarStorage;

    public function __construct(AccessControl $accessControl, AvatarStorage $avatarStorage)
    {
        $this->accessControl = $accessControl;
        $this->avatarStorage = $avatarStorage;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        $this->avatarStorage->put(
            $this->accessControl->getUser()->getId(),
            $event->getRequest()->getUploadedFile('file')
        );

        $event->getResponse()->setStatus(new HttpStatus(HttpStatus::NO_CONTENT));
    }
}
