<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Form\User\CreateUserForm;
use Nrg\Auth\UseCase\User\AssignUserRoles;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class UserRolesAssign.
 */
class UserRolesAssign implements Observer
{
    use ObserverStub;

    /**
     * @var CreateUserForm
     */
    private $form;

    /**
     * @var AssignUserRoles
     */
    private $assignUserRoles;

    public function __construct(CreateUserForm $form, AssignUserRoles $assignUserRoles)
    {
        $this->form = $form;
        $this->assignUserRoles = $assignUserRoles;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->assignUserRoles->execute($event->getRequest()->getBodyParams());

        /*$this->form->populate($event->getRequest()->getBodyParams());
        if ($this->form->hasErrors()) {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY));
            $event->getResponse()->setBody($this->form->serialize());
        } else {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::CREATED));
            $event->getResponse()->setBody($this->assignUserRoles->execute($this->form->serialize()));
        }*/
    }
}
