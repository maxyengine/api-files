<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Abstraction\AvatarStorage;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;
use Exception;

/**
 * Class UserAvatar
 */
class UserAvatar implements Observer
{
    use ObserverStub;

    /**
     * @var AvatarStorage
     */
    private $avatarStorage;

    /**
     * @var string|null
     */
    private $filePath;

    /**
     * @param AvatarStorage $avatarStorage
     */
    public function __construct(AvatarStorage $avatarStorage)
    {
        $this->avatarStorage = $avatarStorage;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext($event)
    {
        $this->filePath = $this->avatarStorage->getPath($event->getRequest()->getQueryParam('id'));

        if (null === $this->filePath) {
            $event->getResponse()->setStatus(new HttpStatus(HttpStatus::NOT_FOUND));

            return;
        }

        $event->getResponse()->setHeader('Content-Type', 'image/png');
    }

    public function onComplete()
    {
        if (null === $this->filePath) {
            return;
        }

        if (ob_get_level()) {
            ob_end_clean();
        }

        readfile($this->filePath);
    }
}
