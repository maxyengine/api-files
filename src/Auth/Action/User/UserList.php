<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\UseCase\User\ListUser;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class UserList.
 */
class UserList implements Observer
{
    use ObserverStub;

    /**
     * @var ListUser
     */
    private $listUser;

    /**
     * UserList constructor.
     *
     * @param ListUser $listUser
     */
    public function __construct(ListUser $listUser)
    {
        $this->listUser = $listUser;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $event->getResponse()->setStatus(new HttpStatus(HttpStatus::OK));
        $event->getResponse()->setBody($this->listUser->execute($event->getRequest()->getBodyParams()));
    }
}
