<?php

namespace Nrg\Auth\Observer\HttpExchange;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Rx\Abstraction\Observer;
use Nrg\Rx\Service\ObserverStub;

/**
 * Class Authorization
 */
class Authorization implements Observer
{
    use ObserverStub;

    /**
     * @var AccessControl
     */
    private $accessControl;

    /**
     * @param AccessControl $accessControl
     */
    public function __construct(AccessControl $accessControl)
    {
        $this->accessControl = $accessControl;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->accessControl->authorization($event->getRequest(), $event->getResponse());
    }
}
