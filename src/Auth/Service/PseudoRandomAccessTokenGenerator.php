<?php

namespace Nrg\Auth\Service;

use DateTime;
use Exception;
use Nrg\Auth\Abstraction\AccessTokenGenerator;
use Nrg\Auth\Entity\Token;
use Nrg\Auth\Entity\User;
use Nrg\Utility\Abstraction\Settings;

/**
 * Class PseudoRandomTokenGenerator
 */
class PseudoRandomAccessTokenGenerator implements AccessTokenGenerator
{
    private const DEFAULT_TOKEN_LENGTH = 128;
    private const DEFAULT_TIME_TO_LIVE = '+24 hour';

    /**
     * @var int
     */
    private $tokenLength;

    private $timeToLive;

    /**
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $config = $settings->getConfig(AccessTokenGenerator::class);
        $this->tokenLength = $config->get('tokenLength', self::DEFAULT_TOKEN_LENGTH);
        $this->timeToLive = $config->get('timeToLive', self::DEFAULT_TIME_TO_LIVE);
    }

    /**
     * {@inheritdoc}
     */
    public function generate(User $user): Token
    {
        $token = new Token($this->generateId());

        $token->populateObject(
            [
                'createdAt' => new DateTime(),
                'expires' => (new DateTime())->modify($this->timeToLive),
                'user' => $user,
            ]
        );

        return $token;
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    private function generateId(): string
    {
        return bin2hex(random_bytes((int)($this->tokenLength / 2)));
    }
}