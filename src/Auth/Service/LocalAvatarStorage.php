<?php

namespace Nrg\Auth\Service;

use Nrg\Auth\Abstraction\AvatarStorage;
use Nrg\Http\Abstraction\RouteProvider;
use Nrg\Http\Value\UploadedFile;
use Nrg\Http\Value\Url;
use Nrg\Utility\Abstraction\Config;
use Nrg\Utility\Abstraction\Settings;

/**
 * Class LocalAvatarStorage
 */
class LocalAvatarStorage implements AvatarStorage
{
    /**
     * @var RouteProvider
     */
    private $routeProvider;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param RouteProvider $routeProvider
     * @param Settings $settings
     */
    public function __construct(RouteProvider $routeProvider, Settings $settings)
    {
        $this->routeProvider = $routeProvider;
        $this->config = $settings->getConfig(AvatarStorage::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl(string $userId): ?Url
    {
        $route = $this->config->get('route');
        $path = "{$this->config->get('path')}/{$userId}.png";

        return file_exists($path) ?
            $this->routeProvider->createUrl($route, ['id' => $userId]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(string $userId): ?string
    {
        return "{$this->config->get('path')}/{$userId}.png";
    }

    /**
     * {@inheritdoc}
     */
    public function put(string $userId, UploadedFile $uploadedFile): void
    {
        $uploadedFile->moveTo("{$this->config->get('path')}/{$userId}.png");
    }
}
