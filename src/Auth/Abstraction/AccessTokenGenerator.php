<?php

namespace Nrg\Auth\Abstraction;

use Exception;
use Nrg\Auth\Entity\Token;
use Nrg\Auth\Entity\User;

/**
 * Interface AccessTokenGenerator
 */
interface AccessTokenGenerator
{
    /**
     * @param User $user
     *
     * @return Token
     * @throws Exception
     */
    public function generate(User $user): Token;
}