<?php

namespace Nrg\Auth\Abstraction;

use Nrg\Auth\Entity\Token;
use Nrg\Auth\Entity\User;
use Nrg\Http\Value\HttpRequest;
use Nrg\Http\Value\HttpResponse;

/**
 * Interface AccessControl
 */
interface AccessControl
{
    public const HEADER_NAME = 'Authorization';

    /**
     * @param HttpRequest $request
     * @param HttpResponse $response
     */
    public function authorization(HttpRequest $request, HttpResponse $response): void;

    /**
     * @return bool
     */
    public function hasToken(): bool;

    /**
     * @return Token
     */
    public function getToken(): Token;

    /**
     * @return User
     */
    public function getUser(): User;
}