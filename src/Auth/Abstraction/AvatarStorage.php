<?php

namespace Nrg\Auth\Abstraction;

use Nrg\Http\Value\UploadedFile;
use Nrg\Http\Value\Url;

/**
 * Interface AvatarStorage
 */
interface AvatarStorage
{
    /**
     * @param string $userId
     *
     * @return Url|null
     */
    public function getUrl(string $userId): ?Url;

    /**
     * @param string $userId
     *
     * @return null|string
     */
    public function getPath(string $userId): ?string;

    /**
     * @param string       $userId
     * @param UploadedFile $uploadedFile
     */
    public function put(string $userId, UploadedFile $uploadedFile): void;
}